import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'data_provider.dart';
import 'data_service.dart';
import 'performance_optimized_screen.dart';
/**
优化和解释

代码结构

	1.	DataService
	•	模拟数据源，提供分页数据。
	2.	DataProvider
	•	负责管理数据逻辑，继承 ChangeNotifier 实现状态通知。
	•	包括分页逻辑、数据缓存、加载状态。
	3.	OptimizedAppWithProvider
	•	使用 ChangeNotifierProvider 提供全局状态，管理 DataProvider。
	4.	PerformanceOptimizedScreen
	•	使用 ListView.builder 构建懒加载列表，动态加载更多数据。

性能优化

	1.	懒加载
使用 ListView.builder 只渲染当前可见区域的组件，减少内存占用。
	2.	状态分离
数据逻辑集中在 DataProvider，界面专注于 UI 构建，提升可维护性。
	3.	异步操作
使用 Future 和 async/await 分离主线程和网络延迟，避免阻塞。
	4.	增量加载
滚动到列表底部时加载下一页数据，而不是一次性加载所有数据。
	5.	Provider 状态管理
使用 Provider 动态更新 UI，仅刷新必要的组件。

项目运行效果

	•	页面加载时会显示第一页数据。
	•	滑动到列表底部时会自动加载更多。
	•	显示加载指示器，并在没有更多数据时显示“没有更多数据了”提示。

 */

/// 使用 Provider 管理状态的主应用
class OptimizedAppWithProvider extends StatelessWidget {
  const OptimizedAppWithProvider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ChangeNotifierProvider(
        // 提供 DataProvider 的实例给子组件
        create: (_) => DataProvider(dataService: DataService()),
        child: const PerformanceOptimizedScreen(),
      ),
    );
  }
}

void main() {
  runApp(const OptimizedAppWithProvider());
}
