import 'dart:async';

import 'package:flutter/material.dart';

class CompleterFutureExample extends StatefulWidget {
  const CompleterFutureExample({Key? key}) : super(key: key);

  @override
  _CompleterFutureExampleState createState() => _CompleterFutureExampleState();
}

/*
  我们将使用 Completer 来模拟一个异步操作（例如，模拟一个耗时的任务），然后在完成时通知调用者，最后更新界面显示结果。

  步骤：
    1、创建一个 Completer 来手动完成 Future。
    2、模拟一个耗时操作（例如延时操作），然后完成 Future。
    3、使用 Completer 来控制 Future 的完成时机。
 */
class _CompleterFutureExampleState extends State<CompleterFutureExample> {
// 模拟一个自定义 Future 的异步方法
  Future<String> fetchDataWithCompleter() {
    // 创建一个 Completer
    Completer<String> completer = Completer<String>();

    // 模拟异步操作，延迟 3 秒
    Future.delayed(const Duration(seconds: 3), () {
      // 操作完成后，手动完成 Completer
      completer.complete("Data Loaded Successfully!");
    });

    // 返回 completer 的 Future
    return completer.future;
  }

  late Future<String> _dataFuture;

  @override
  void initState() {
    super.initState();
    // 在初始化时调用自定义的 Future
    _dataFuture = fetchDataWithCompleter();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Completer Example')),
      body: FutureBuilder<String>(
        future: _dataFuture, // 使用自定义的 Future
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // 正在加载
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            // 错误处理
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData) {
            // 数据为空
            return const Center(child: Text('No data available.'));
          } else {
            // 数据加载成功
            return Center(child: Text(snapshot.data!));
          }
        },
      ),
    );
  }
}
