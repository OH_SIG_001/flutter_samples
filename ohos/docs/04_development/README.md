# 功能开发

## 在OpenHarmony应用中添加Flutter页面

1. [如何使用 FlutterPage](./如何使用%20FlutterPage.md)
2. [如何使用混合开发添加跳转 FlutterEntry](./如何使用混合开发添加跳转%20FlutterEntry.md)

## 在OpenHarmony应用中使用Flutter Channel能力

1. [如何使用Flutter与OpenHarmony通信 FlutterChannel](./如何使用Flutter与OpenHarmony通信%20FlutterChannel.md)

## 与原生混合渲染

1. [Flutter OHOS外接纹理适配简介.md](./Flutter%20OHOS外接纹理适配简介.md)
2. [如何使用多引擎 FlutterEngineGroup](./如何使用多引擎%20FlutterEngineGroup.md)
3. [如何使用PlatformView](./如何使用PlatformView.md)
4. [PlatformView同层渲染方案适配切换指导](./PlatformView同层渲染方案适配切换指导.md)

## 开发Flutter静态库

1. [开发module](./如何使用混合开发%20module.md)
2. [开发package](./开发package.md)
3. [开发plugin](./开发plugin.md)
4. [开发FFI plugin](./开发FFI%20plugin.md)
