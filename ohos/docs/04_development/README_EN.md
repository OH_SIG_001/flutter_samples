# Feature Development

## Adding a Flutter Page to Your OpenHarmony Application

1. [Using FlutterPage](./using-flutterpage.md)
2. [Using FlutterEntry in Hybrid Development](./using-flutterEntry-in-hybrid-development.md)

## Using a Flutter Channel in Your OpenHarmony Application

1. [Using a Flutter Channel for Communication Between Flutter and OpenHarmony](./using-a-flutter-channel-for-communication-between-flutter-and-openharmony.md)

## Hybrid Rendering with Native Code

1. [External Texture Adaptation for Flutter](./external-texture-adaptation-for-flutter.md)
2. [Using FlutterEngineGroup](./using-flutterenginegroup.md)
3. [Using a Platform View](./using-a-platform-view.md)
4. [Platform View Same-Layer Rendering Adaptation and Switching](./platform-view-same-layer-rendering-solution-adaptation-and-switching.md)

## Developing a Flutter Static Library

1. [Using a Module in Hybrid Development](./using-a-module-in-hybrid-development.md)
2. [Developing a Package](./developing-a-package.md)
3. [Developing a Plugin](./developing-a-plugin.md)
4. [Developing an FFI Plugin](./developing-an-ffi-plugin.md)
