/*
 * Copyright (c) 2023 Hunan OpenValley Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

typedef OnViewCreated = Function(LoginViewController);

///一键登录
class HuaweiLoginView extends StatefulWidget {
  final OnViewCreated onViewCreated;

  const HuaweiLoginView(this.onViewCreated, {Key? key}) : super(key: key);

  @override
  State<HuaweiLoginView> createState() => _HuaweiLoginViewState();
}

class _HuaweiLoginViewState extends State<HuaweiLoginView> {
  late MethodChannel _channel;

  @override
  Widget build(BuildContext context) {
    return _getPlatformFaceView();
  }

  Widget _getPlatformFaceView() {
    return OhosView(
      viewType: 'com.example.flutter_login/loginView',
      onPlatformViewCreated: _onPlatformViewCreated,
      creationParams: const <String, dynamic>{'initParams': 'hello world'},
      creationParamsCodec: const StandardMessageCodec(),
    );
  }

  void _onPlatformViewCreated(int id) {
    _channel = MethodChannel('com.example.flutter_login/loginView$id');
    final controller = LoginViewController._(
      _channel,
    );
    widget.onViewCreated(controller);
  }
}

class LoginViewController {
  final MethodChannel _channel;
  final StreamController<String> _controller = StreamController<String>();

  LoginViewController._(
    this._channel,
  ) {
    _channel.setMethodCallHandler(
      (call) async {
        // 从ohos端获取数据
        final result = call.arguments as String;

        switch (call.method) {
          case 'quickLoginAnonymousPhone':
            {
              // 获取匿名手机号
              final data = {
                'method': call.method,
                'data': result,
              };
              _controller.sink.add(jsonEncode(data));
              break;
            }
          case 'loginSuccess':
            {
              // 一键登录成功
              final data = {
                'method': call.method,
                'data': result,
              };
              _controller.sink.add(jsonEncode(data));
              Fluttertoast.showToast(msg: result);
              break;
            }
          case '1005300001':
            {

              final data = {
                'method': call.method,
                'data': result,
              };
              _controller.sink.add(jsonEncode(data));

              break;
            }
          default:
            {
              Fluttertoast.showToast(msg: result);
              break;
            }
        }
      },
    );
  }

  Stream<String> get customDataStream => _controller.stream;




  setListener(Function function){

  }

  // 发送数据给ohos
  Future<void> sendMessageToOhosView(String method, String message) async {
    await _channel.invokeMethod(
      method,
      message,
    );
  }

  // 发消息给ohos更新同意协议状态
  Future<void> setAgreementStatus(bool v) async {
    await _channel.invokeMethod(
      'setAgreementStatus',
      v,
    );
  }

  // 同意协议并登录
  Future<void> agreementAndLogin() async {
    await _channel.invokeMethod(
      'agreementAndLogin',
      true,
    );
  }
}