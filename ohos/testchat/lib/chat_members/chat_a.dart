import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatA extends StatelessWidget {
  const ChatA({
    Key? key,
    required this.text,
  }) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(6.0, 2, 10.0, 2,),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Flexible(
            child: CircleAvatar(
                radius: 25,
                backgroundColor: Colors.white,
                backgroundImage: AssetImage(
                  "assets/images/chat_a.png",
                )
            ),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
          ),
          Flexible(
            child: DecoratedBox(
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 205, 155, 29),
                borderRadius: BorderRadius.circular(16),
              ),
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: Text(
                  text,
                  style: const TextStyle(
                    fontSize: 14,
                    color: Color.fromARGB(255, 48, 14, 43),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}