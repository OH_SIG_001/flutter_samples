import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatC extends StatelessWidget {
  const ChatC({
    Key? key,
    required this.id
  }) : super(key: key);
  final int id;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const CircleAvatar(
            radius: 25,
            backgroundColor: Colors.white,
            backgroundImage: AssetImage("assets/images/chat_c.png")
        ),
        const Padding(
          padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
        ),
        // Container(
        //   alignment: Alignment.center,
        //   // width: 500,
        //   // height: 500,
        //   child: Image(
        //     image: AssetImage(id % 2 == 0 ? "assets/images/chat.jpeg" : "assets/images/chat2.jpeg"),
        //     width: 200.0,
        //     height: 200.0,
        //   ),
        // ),

        Image(
          image: AssetImage(id % 2 == 0 ? "assets/images/chat.jpeg" : "assets/images/chat2.jpeg"),
          width: 200.0,
          height: 200.0,
        )
      ],
    );
  }
}