import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './WebViewPage.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:go_router/go_router.dart';

import 'generated/l10n.dart';

void main() {
  runApp(const MyApp());
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

var router = GoRouter(
  initialLocation: '/',
  navigatorKey:navigatorKey,
  routes: [
    GoRoute(
      name: 'home',
      path: '/',
      builder: (context, state) =>  const MyHomePage(title: 'WebView Demo'),
    ),
    GoRoute(
        name: 'web',
        path: "/web",
        builder: (BuildContext context, GoRouterState state) {
          return const WebViewPage();
        }
    ),
  ],
);

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {

    return CupertinoApp.router(
      routerConfig: router,
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: S.delegate.supportedLocales,
    );
  }
}



class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            GestureDetector(
              onTap: () {
                context.push("/web");
              },
              child: const Text(
                '点击 启动一个 本地H5',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
