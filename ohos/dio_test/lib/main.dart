import 'dart:io';

import 'package:dio_test/src/AdaptersTestPage.dart';
import 'package:dio_test/src/BasicTestPage.dart';
import 'package:dio_test/src/CancelTokenTest.dart';
import 'package:dio_test/src/DioMixinTestPage.dart';
import 'package:dio_test/src/DownloadTestPage.dart';
import 'package:dio_test/src/EncodingTestPage.dart';
import 'package:dio_test/src/ExampleTestPage.dart';
import 'package:dio_test/src/ExceptionTestPage.dart';
import 'package:dio_test/src/FormdataTestPage.dart';
import 'package:dio_test/src/InterceptorTestPage.dart';
import 'package:dio_test/src/MimetypeTestPage.dart';
import 'package:dio_test/src/MultipartFileTestPage.dart';
import 'package:dio_test/src/OptionsTestPage.dart';
import 'package:dio_test/src/PinningTestPage.dart';
import 'package:dio_test/src/RequestIntegrationTestPage.dart';
import 'package:dio_test/src/StacktraceTestPage.dart';
import 'package:dio_test/src/TimeoutTestPage.dart';
import 'package:dio_test/src/TransformerTestPage.dart';
import 'package:dio_test/src/UploadTestPage.dart';
import 'package:dio_test/src/UrlEncodedTestPage.dart';
import 'package:flutter/material.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  HttpOverrides.global = GlobalHttpOverrides();

  final app = [
    MainItem('example_test', const ExampleTestPage(title: 'example_test')),
    MainItem('adapters_test', AdaptersTestPage('adapters_test')),
    MainItem('basic_test', BasicTestPage('basic_test')),
    MainItem('cancel_token_test', CancelTokenTestPage('cancel_token_test')),
    MainItem('dio_mixin_test', DioMixinTestPage('dio_mixin_test')),
    MainItem('download_test', DownloadTestPage('download_test')),
    MainItem('encoding_test', EncodingTestPage('encoding_test')),
    MainItem('exception_test', ExceptionTestPage('exception_test')),
    MainItem('formdata_test', FormdataTestPage('formdata_test')),
    MainItem('interceptor_test', InterceptorTestPage('interceptor_test')),
    MainItem('mimetype_test', MimetypeTestPage('mimetype_test')),
    MainItem('multipart_file_test', MultipartFileTestPage('multipart_file_test')),
    MainItem('options_test', OptionsTestPage('options_test')),
    MainItem('pinning_test', PinningTestPage('pinning_test')),
    MainItem('request_integration_test', RequestIntegrationTestPage('request_integration_test')),
    MainItem('stacktrace_test', StacktraceTestPage('stacktrace_test')),
    MainItem('timeout_test', TimeoutTestPage('timeout_test')),
    MainItem('transformer_test', TransformerTestPage('transformer_test')),
    MainItem('upload_test', UploadTestPage('upload_test')),
    MainItem('url_encoded_test', UrlEncodedTestPage('url_encoded_test')),
  ];

  runApp(TestModelApp(
      appName: 'dio',
      data: app));
}

class GlobalHttpOverrides extends HttpOverrides {
  @override
  HttpClient creatHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}