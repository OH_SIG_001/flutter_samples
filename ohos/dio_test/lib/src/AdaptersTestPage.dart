import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio/io.dart';

import '../common/test_page.dart';

class AdaptersTestPage extends TestPage {
  AdaptersTestPage(super.Stitle){
    group(
      '$IOHttpClientAdapter',
          () {
        test('dio.httpClientAdapter = IOHttpClientAdapter(onHttpClientCreate: (){})', () async {
          int onHttpClientCreateInvokeCount = 0;
          final dio = Dio();
          dio.httpClientAdapter = IOHttpClientAdapter(
            onHttpClientCreate: (client) {
              onHttpClientCreateInvokeCount++;
              return client;
            },
          );
          (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (client) {
            client.badCertificateCallback = (cert, host, post) {
              return true;
            };
          };
          await dio.get('https://www.baidu.com/');
          expect(onHttpClientCreateInvokeCount, 1);
        });

        test('dio.httpClientAdapter = IOHttpClientAdapter(createHttpClient: () {})', () async {
          int createHttpClientCount = 0;
          final dio = Dio();
          dio.httpClientAdapter = IOHttpClientAdapter(
            createHttpClient: () {
              createHttpClientCount++;
              HttpClient httpClient = HttpClient();
              httpClient.badCertificateCallback = (cert, host, post) {
                return true;
              };
              return httpClient;
            },
          );
          (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (client) {
            client.badCertificateCallback = (cert, host, post) {
              return true;
            };
          };
          await dio.get('https://www.baidu.com/');
          expect(createHttpClientCount, 1);
        });
      },
    );
  }

}