import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio_test/DioExtens.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider_ohos/path_provider_ohos.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';

import '../common/test_page.dart';
import '../mock/adapters.dart';

class DownloadTestPage extends TestPage {
  final PathProviderPlatform provider = PathProviderOhos();
  final dio = getDio();

  DownloadTestPage(super.title) {
    test('dio.download(urlPath, savePath)', () async {
      Response response = await dio.download('http://img2.baidu.com/it/u=3902309251,2849519907&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889',
          '${await provider.getApplicationCachePath()}/download1.jpeg');

      expect(response.statusCode, '');
    });

    test('dio.downloadUri(serverUrl.replace(path: uri), (header) => fileCacheUrl,)', () async {
      Response response = await dio.downloadUri(
        Uri.parse('http://img2.baidu.com/it/u=3902309251,2849519907&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889'),
            (header) async => '${await provider.getApplicationCachePath()}/download2.jpeg',
      );

      expect(response.statusCode, '');
    });

    test('dio.download(urlPath, savePath).catchError()', () async {
      Response response = await dio
          .download('http://img2.baidu.com/it/u=3902309251,2849519907&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889',
          '${await provider.getApplicationCachePath()}/download3.jpeg')
          .catchError((e) => (e as DioException).response);
      expect(response.data, 'error');
      response = await dio
          .download(
        'http://img2.baidu.com/it/u=3902309251,2849519907&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889',
        '${await provider.getApplicationCachePath()}/download4.jpeg',
        options: Options(receiveDataWhenStatusError: false),
      )
          .catchError((e) => (e as DioException).response);
      expect(response.data, null);
    });

    test('Dio(BaseOptions(receiveTimeout: Duration(milliseconds: 1),baseUrl: serverUrl.toString()))', () async {
      final dio = Dio(
        BaseOptions(
          receiveTimeout: Duration(milliseconds: 1),
        ),
      );
      expect(
        dio.download('http://img2.baidu.com/it/u=3902309251,2849519907&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889',
            '${await provider.getApplicationCachePath()}/download5.jpeg')
            .catchError((e) => throw (e as DioException).type),
        DioExceptionType.receiveTimeout,
      );
    });

    test('dio.download(urlPath, savePath, cancelToken: cancelToken)', () async {
      final cancelToken = CancelToken();
      Future.delayed(Duration(milliseconds: 100), () {
        cancelToken.cancel();
      });
      expect(
        Dio().download(
          'http://img2.baidu.com/it/u=3902309251,2849519907&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889',
          '${await provider.getApplicationCachePath()}/download6.jpeg',
          cancelToken: cancelToken,
        )
            .catchError((e) => throw (e as DioException).type),
        DioExceptionType.cancel,
      );
    });

    test('dio.download(urlPath, savePath, deleteOnError: true, onReceiveProgress: (count, total) => throw AssertionError())', () async {
      expect(
        dio.download(
          'http://img2.baidu.com/it/u=3902309251,2849519907&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889',
          '${await provider.getApplicationCachePath()}/download7.jpeg',
          deleteOnError: true,
          onReceiveProgress: (count, total) => throw AssertionError(),
        )
            .catchError((e) => throw (e as DioException).error!),
        '',
      );
    });

    test('dio.download(urlPath, savePath, deleteOnError: true,cancelToken: cancelToken,onReceiveProgress: (count, total) => cancelToken.cancel(),)',
            () async {
      final cancelToken = CancelToken();
      expect(
        dio.download(
          'http://img2.baidu.com/it/u=3902309251,2849519907&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889',
          '${await provider.getApplicationCachePath()}/download8.jpeg',
          deleteOnError: true,
          cancelToken: cancelToken,
          onReceiveProgress: (count, total) => cancelToken.cancel(),
        )
            .catchError((e) => throw (e as DioException).type),
        DioExceptionType.cancel,
      );
      await Future.delayed(const Duration(milliseconds: 100));
    });

    test('dio.download("/test", (testPath, (headers) => testPath, (headers) async => testPath)))', () async {
      final testPath = p.join(Directory.systemTemp.path, 'dio', 'testPath');

      final dio = getDio()
        ..options.baseUrl = EchoAdapter.mockBase
        ..httpClientAdapter = EchoAdapter();

      expect(
        dio.download('/test', testPath),
        '',
      );
      expect(
        dio.download('/test', (headers) => testPath),
        '',
      );
      expect(
        dio.download('/test', (headers) async => testPath),
        '',
      );
    });
  }

}