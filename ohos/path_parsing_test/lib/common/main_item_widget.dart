import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../common/test_route.dart';

/// Main item widget.
class MainItemWidget extends StatefulWidget {
  /// Main item widget.
  const MainItemWidget(this.item, this.onTap, {Key? key}) : super(key: key);

  /// item data.
  final MainItem item;

  /// onTap action (typically run or open).
  final void Function(MainItem item) onTap;

  @override
  MainItemWidgetState createState() => MainItemWidgetState();
}

class MainItemWidgetState extends State<MainItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: ListTile(
          tileColor: CupertinoColors.extraLightBackgroundGray,
          title: Text(widget.item.title),
          subtitle: Text(widget.item.description),
          onTap: _onTap),
    );
  }

  void _onTap() {
    widget.onTap(widget.item);
  }
}
