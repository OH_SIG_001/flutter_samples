import 'package:flutter/cupertino.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';
import 'pages/example.dart';
import 'pages/function_list.dart';
import 'pages/parse_path_deep_test.dart';
import 'pages/parse_path_test.dart';
import 'pages/s_PathProxy_test.dart';
import 'pages/s_PathSegmentData_test.dart';
import 'pages/s_SvgPathNormalizer_test.dart';
import 'pages/s_SvgPathStringSource_test.dart';

void main() {
  final items = [
    MainItem('functions', "", route: "FunctionList"),
    // MainItem('excample_test', "", route: "PathParsingPage"),
    MainItem('s_PathProxy_test', "", route: "SPathProxyTestPage"),
    MainItem('s_PathSegmentData_test', "", route: "SPathSegmentDataTestPage"),
    MainItem('s_SvgPathNormalizer_test', "", route: "SSvgPathNormalizerTestPage"),
    MainItem('s_SvgPathStringSource_test', "", route: "SSvgPathStringSourceTestPage"),

    //SSvgPathStringSourceTestPage
    MainItem('parse_path_deep_test', "", route: "ParsePathDeepTestPage"),
    MainItem('parse_path_test', "", route: "ParsePathTestPage"),
  ];

  runApp(TestModelApp(
      appName: 'parse_path',
      data: TestRoute(
        items: items,
        routes: {
          "FunctionList": (context) {
            return FunctionList();
          },
          // "PathParsingPage": (context) {
          //   return PathParsingPage();
          // },
          "SPathProxyTestPage": (context) {
            return SPathProxyTestPage('SPathProxyTestPage');
          },
          "SPathSegmentDataTestPage": (context) {
            return SPathSegmentDataTestPage('SPathSegmentDataTestPage');
          },
          "SSvgPathNormalizerTestPage": (context) {
            return SSvgPathNormalizerTestPage('SSvgPathNormalizerTestPage');
          },
          "SSvgPathStringSourceTestPage": (context) {
            return SSvgPathStringSourceTestPage('SSvgPathStringSourceTestPage');
          },

          //SSvgPathStringSourceTestPage
          "ParsePathDeepTestPage": (context) {
            return ParsePathDeepTestPage("ParsePathDeepTestPage");
          },
          "ParsePathTestPage": (context) {
            return ParsePathTestPage("ParsePathTestPage");
          },
        },
        //PathParsingExamp
      ))); //BuilderTestPage
}
