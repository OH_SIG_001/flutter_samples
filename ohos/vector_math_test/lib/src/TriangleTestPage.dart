import 'package:vector_math/vector_math.dart';
import 'package:vector_math_test/src/test_utils.dart';

import '../common/test_page.dart';

class TriangleTestPage extends TestPage{
  TriangleTestPage(super.title) {
    group('Triangle', () {
      test('Triangle.points().copyNormalInto(Vector3 normal)', testCopyNormalInto);
    });
  }

  void testCopyNormalInto() {
    final triangle = Triangle.points(
        Vector3(1.0, 0.0, 1.0), Vector3(0.0, 2.0, 1.0), Vector3(1.0, 2.0, 0.0));
    final normal = Vector3.zero();

    triangle.copyNormalInto(normal);

    relativeTest(normal, Vector3(-0.666666666, -0.333333333, -0.666666666));
  }

}