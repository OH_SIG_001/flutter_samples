import 'package:vector_math/vector_math.dart';
import 'package:vector_math_test/src/test_utils.dart';

import '../common/test_page.dart';

class SphereTestPage extends TestPage{
  SphereTestPage(super.title) {
    group('Sphere', () {
      test('Sphere.centerRadius().containsVector3(Vector3 other)', testSphereContainsVector3);
      test('Sphere.centerRadius().intersectsWithVector3(Vector3 other)', testSphereIntersectionVector3);
      test('Sphere.centerRadius().intersectsWithSphere(Sphere other)', testSphereIntersectionSphere);
    });
  }

  void testSphereContainsVector3() {
    final parent = Sphere.centerRadius($v3(1.0, 1.0, 1.0), 2.0);
    final child = $v3(1.0, 1.0, 2.0);
    final cutting = $v3(1.0, 3.0, 1.0);
    final outside = $v3(-10.0, 10.0, 10.0);

    expect(parent.containsVector3(child), true);
    expect(parent.containsVector3(cutting), false);
    expect(parent.containsVector3(outside), false);
  }

  void testSphereIntersectionVector3() {
    final parent = Sphere.centerRadius($v3(1.0, 1.0, 1.0), 2.0);
    final child = $v3(1.0, 1.0, 2.0);
    final cutting = $v3(1.0, 3.0, 1.0);
    final outside = $v3(-10.0, 10.0, 10.0);

    expect(parent.intersectsWithVector3(child), true);
    expect(parent.intersectsWithVector3(cutting), true);
    expect(parent.intersectsWithVector3(outside), false);
  }

  void testSphereIntersectionSphere() {
    final parent = Sphere.centerRadius($v3(1.0, 1.0, 1.0), 2.0);
    final child = Sphere.centerRadius($v3(1.0, 1.0, 2.0), 1.0);
    final cutting = Sphere.centerRadius($v3(1.0, 6.0, 1.0), 3.0);
    final outside = Sphere.centerRadius($v3(10.0, -1.0, 1.0), 1.0);

    expect(parent.intersectsWithSphere(child), true);
    expect(parent.intersectsWithSphere(cutting), true);
    expect(parent.intersectsWithSphere(outside), false);
  }

}