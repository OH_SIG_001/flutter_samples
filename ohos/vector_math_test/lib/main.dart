import 'package:flutter/cupertino.dart';
import 'package:vector_math_test/src/Aabb2TestPage.dart';
import 'package:vector_math_test/src/Aabb3TestPage.dart';
import 'package:vector_math_test/src/ColorsTestPage.dart';
import 'package:vector_math_test/src/ExamplePage.dart';
import 'package:vector_math_test/src/FrustumTestPage.dart';
import 'package:vector_math_test/src/GeometryTestPage.dart';
import 'package:vector_math_test/src/Matrix2TestPage.dart';
import 'package:vector_math_test/src/Matrix3TestPage.dart';
import 'package:vector_math_test/src/Matrix4TestPage.dart';
import 'package:vector_math_test/src/NoiseTestPage.dart';
import 'package:vector_math_test/src/Obb3TestPage.dart';
import 'package:vector_math_test/src/OpenglMatrixTestPage.dart';
import 'package:vector_math_test/src/PlaneTestPage.dart';
import 'package:vector_math_test/src/QuadTestPage.dart';
import 'package:vector_math_test/src/QuaternionTestPage.dart';
import 'package:vector_math_test/src/RayTestPage.dart';
import 'package:vector_math_test/src/ScalarListViewTestPage.dart';
import 'package:vector_math_test/src/SphereTestPage.dart';
import 'package:vector_math_test/src/TriangleTestPage.dart';
import 'package:vector_math_test/src/UtilitiesTestPage.dart';
import 'package:vector_math_test/src/Vector2ListTestPage.dart';
import 'package:vector_math_test/src/Vector2TestPage.dart';
import 'package:vector_math_test/src/Vector3ListTestPage.dart';
import 'package:vector_math_test/src/Vector3TestPage.dart';
import 'package:vector_math_test/src/Vector4ListTestPage.dart';
import 'package:vector_math_test/src/Vector4TestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('example_test', const ExamplePage(title: 'example_test')),
    MainItem('aabb2_test', Aabb2TestPage('aabb2_test')),
    MainItem('aabb3_test', Aabb3TestPage('aabb3_test')),
    MainItem('colors_test', ColorsTestPage('colors_test')),
    MainItem('frustum_test', FrustumTestPage('frustum_test')),
    MainItem('geometry_test', GeometryTestPage('geometry_test')),
    MainItem('matrix2_test', Matrix2TestPage('matrix2_test')),
    MainItem('matrix3_test', Matrix3TestPage('matrix3_test')),
    MainItem('matrix4_test', Matrix4TestPage('matrix4_test')),
    MainItem('noise_test', NoiseTestPage('noise_test')),
    MainItem('obb3_test', Obb3TestPage('obb3_test')),
    MainItem('opengl_Matrix_test', OpenglMatrixTestPage('opengl_Matrix_test')),
    MainItem('plane_test', PlaneTestPage('plane_test')),
    MainItem('quad_test', QuadTestPage('quad_test')),
    MainItem('quaternion_test', QuaternionTestPage('quaternion_test')),
    MainItem('ray_test', RayTestPage('ray_test')),
    MainItem('scalar_list_view_test', ScalarListViewTestPage('scalar_list_view_test')),
    MainItem('sphere_test', SphereTestPage('sphere_test')),
    MainItem('triangle_test', TriangleTestPage('triangle_test')),
    MainItem('utilities_test', UtilitiesTestPage('utilities_test')),
    MainItem('vector2_list_test', Vector2ListTestPage('vector2_list_test')),
    MainItem('vector2_test', Vector2TestPage('vector2_test')),
    MainItem('vector3_list_test', Vector3ListTestPage('vector3_list_test')),
    MainItem('vector3_test', Vector3TestPage('vector3_test')),
    MainItem('vector4_list_test', Vector4ListTestPage('vector4_list_test')),
    MainItem('vector4_test', Vector4TestPage('vector4_test')),
  ];

  runApp(TestModelApp(
      appName: 'vector_math',
      data: app));
}