import 'package:clock/clock.dart';
import 'package:clock_test/src/utils.dart';

import '../common/test_page.dart';

class StopwatchTestPage extends TestPage {
  StopwatchTestPage(super.title) {
    test('fixed(1990, 11, 8).stopwatch().frequency', () {
      expect(fixed(1990, 11, 8).stopwatch().frequency, Stopwatch().frequency);
    });

    group('before it starts', () {
      test('clock.stopwatch().isRunning', () {
        Stopwatch stopwatch = clock.stopwatch();
        expect(stopwatch.isRunning, 'isFalse');
      });

      test(
          '测试使用clock.stopwatch().stop()后clock.stopwatch().isRunning, clock.stopwatch().elapsed的情况',
          () {
        Stopwatch stopwatch = clock.stopwatch();
        stopwatch.stop();
        expect(stopwatch.isRunning, 'isFalse');
        expect(stopwatch.elapsed, Duration.zero);
      });

      group('测试当前时间的秒表有没有时间流逝', () {
        Stopwatch stopwatch = clock.stopwatch();
        test('持续时间stopwatch.elapsed',
            () => expect(stopwatch.elapsed, Duration.zero));
        test('秒表走动声音次数stopwatch.elapsedTicks',
            () => expect(stopwatch.elapsedTicks, 'isZero'));
        test('微秒stopwatch.elapsedMicroseconds',
            () => expect(stopwatch.elapsedMicroseconds, 'isZero'));
        test('毫秒stopwatch.elapsedMilliseconds',
            () => expect(stopwatch.elapsedMilliseconds, 'isZero'));
      });
    });

    group('当经过12345微秒时', () {
      group('并且秒表处于活动状态', () {
        test(
            'Clock(() => time).stopwatch()..start(), Clock(() => time).microsFromNow(12345)是否在运行',
            () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          expect(stopwatch.isRunning, 'isTrue');
        });

        test('报告更多的运行时间', () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          time = clock.microsFromNow(54321);
          expect(stopwatch.elapsedMicroseconds, 66666);
        });

        test('stopwatch.start()不起作用', () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          stopwatch.start();
          expect(stopwatch.isRunning, 'isTrue');
          expect(stopwatch.elapsedMicroseconds, 12345);
        });

        group('重置', () {
          test('stopwatch.reset()将运行时间设置为零', () {
            DateTime time = date(1990, 11, 8);
            Clock clock = Clock(() => time);
            Stopwatch stopwatch = clock.stopwatch()..start();
            time = clock.microsFromNow(12345);
            stopwatch.reset();
            expect(stopwatch.elapsed, Duration.zero);
          });

          test('stopwatch.reset()之后设置更多的运行时间', () {
            DateTime time = date(1990, 11, 8);
            Clock clock = Clock(() => time);
            Stopwatch stopwatch = clock.stopwatch()..start();
            time = clock.microsFromNow(12345);
            stopwatch.reset();
            time = clock.microsFromNow(54321);
            expect(stopwatch.elapsedMicroseconds, 54321);
          });
        });

        group('报告已经过去的时间', () {
          test('已持续时间', () {
            DateTime time = date(1990, 11, 8);
            Clock clock = Clock(() => time);
            Stopwatch stopwatch = clock.stopwatch()..start();
            time = clock.microsFromNow(12345);
            expect(stopwatch.elapsed, const Duration(microseconds: 12345));
          });

          test('发出滴答声次数', () {
            DateTime time = date(1990, 11, 8);
            Clock clock = Clock(() => time);
            Stopwatch stopwatch = clock.stopwatch()..start();
            time = clock.microsFromNow(12345);
            expect(stopwatch.elapsedTicks,
                (Stopwatch().frequency * 12345) ~/ 1000000);
          });

          test('已过去的微秒数', () {
            DateTime time = date(1990, 11, 8);
            Clock clock = Clock(() => time);
            Stopwatch stopwatch = clock.stopwatch()..start();
            time = clock.microsFromNow(12345);
            expect(stopwatch.elapsedMicroseconds, 12345);
          });

          test('已过去的毫秒数', () {
            DateTime time = date(1990, 11, 8);
            Clock clock = Clock(() => time);
            Stopwatch stopwatch = clock.stopwatch()..start();
            time = clock.microsFromNow(12345);
            expect(stopwatch.elapsedMilliseconds, 12);
          });
        });
      });

      test('使用stopwatch.stop()后，stopwatch.isRunning的情况', () {
        DateTime time = date(1990, 11, 8);
        Clock clock = Clock(() => time);
        Stopwatch stopwatch = clock.stopwatch()..start();
        time = clock.microsFromNow(12345);
        stopwatch.stop();
        expect(stopwatch.isRunning, 'isFalse');
      });

      test("使用stopwatch.stop()设置clock.microsFromNow(54321)查看stopwatch.elapsedMicroseconds情况", () {
        DateTime time = date(1990, 11, 8);
        Clock clock = Clock(() => time);
        Stopwatch stopwatch = clock.stopwatch()..start();
        time = clock.microsFromNow(12345);
        stopwatch.stop();
        time = clock.microsFromNow(54321);
        expect(stopwatch.elapsedMicroseconds, 12345);
      });

      test('设置clock.microsFromNow(12345)后停止再开始，再设置clock.microsFromNow(54321)，查看stopwatch.elapsedMicroseconds情况', () {
        DateTime time = date(1990, 11, 8);
        Clock clock = Clock(() => time);
        Stopwatch stopwatch = clock.stopwatch()..start();
        time = clock.microsFromNow(12345);
        stopwatch.stop();
        stopwatch.start();
        expect(stopwatch.isRunning, 'isTrue');
        time = clock.microsFromNow(54321);
        expect(stopwatch.elapsedMicroseconds, 66666);
      });

      group('reset()', () {
        test('stopwatch.reset()后查看stopwatch.elapsed', () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          stopwatch.reset();
          expect(stopwatch.elapsed, Duration.zero);
        });

        test("stopwatch.reset()后重新设置clock.microsFromNow(54321)后查看stopwatch.elapsed", () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          stopwatch.reset();
          time = clock.microsFromNow(54321);
          expect(stopwatch.elapsed, Duration.zero);
        });
      });

      group('报告已过去的时间', () {
        test('已过去的持续时间', () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          expect(stopwatch.elapsed, const Duration(microseconds: 12345));
        });

        test('秒表发出滴答声次数', () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          expect(stopwatch.elapsedTicks,
              (Stopwatch().frequency * 12345) ~/ 1000000);
        });

        test('已过去微秒数', () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          expect(stopwatch.elapsedMicroseconds, 12345);
        });

        test('已过去毫秒数', () {
          DateTime time = date(1990, 11, 8);
          Clock clock = Clock(() => time);
          Stopwatch stopwatch = clock.stopwatch()..start();
          time = clock.microsFromNow(12345);
          expect(stopwatch.elapsedMilliseconds, 12);
        });
      });
    });
  }
}
