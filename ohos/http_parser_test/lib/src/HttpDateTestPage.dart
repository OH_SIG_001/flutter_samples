import 'package:http_parser/http_parser.dart';

import '../common/test_page.dart';

class HttpDateTestPage extends TestPage{
  HttpDateTestPage(super.title) {
    group('format', () {
      test('formatHttpDate(DateTime.utc(2014, 9, 9, 9, 9, 9), parseHttpDate(formatHttpDate(DateTime.utc(2014, 9, 9, 9, 9, 9)))', () {
        final date = DateTime.utc(2014, 9, 9, 9, 9, 9);
        final formatted = formatHttpDate(date);

        expect(formatted, 'Tue, 09 Sep 2014 09:09:09 GMT');
        final parsed = parseHttpDate(formatted);

        expect(parsed, date);
      });

      test('formatHttpDate(DateTime.utc(1999, 12, 31, 23, 59, 59)), parseHttpDate(formatHttpDate(DateTime.utc(1999, 12, 31, 23, 59, 59))))', () {
        final date = DateTime.utc(1999, 12, 31, 23, 59, 59);
        final formatted = formatHttpDate(date);

        expect(formatted, 'Fri, 31 Dec 1999 23:59:59 GMT');
        final parsed = parseHttpDate(formatted);

        expect(parsed, date);
      });

      test('formatHttpDate(DateTime.utc(2000)), parseHttpDate(formatHttpDate(DateTime.utc(2000)))', () {
        final date = DateTime.utc(2000);
        final formatted = formatHttpDate(date);

        expect(formatted, 'Sat, 01 Jan 2000 00:00:00 GMT');
        final parsed = parseHttpDate(formatted);

        expect(parsed, date);
      });
    });

    group('parse', () {
      group('RFC 1123', () {
        test('parseHttpDate("Sun, 06 Nov 1994 08:49:37 GMT")', () {
          final date = parseHttpDate('Sun, 06 Nov 1994 08:49:37 GMT');
          expect(date.day, (6));
          expect(date.month, (DateTime.november));
          expect(date.year, (1994));
          expect(date.hour, (8));
          expect(date.minute, (49));
          expect(date.second, (37));
          expect(date.timeZoneName, ('UTC'));
        });

        test('parseHttpDate("Sun,06 Nov 1994 08:49:37 GMT")参数不同格式是否能正常', () {
          expect(() => parseHttpDate('Sun,06 Nov 1994 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06Nov 1994 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov1994 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov 199408:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov 1994 08:49:37GMT'),
              '');
        });

        test('parseHttpDate("Sun,  06 Nov 1994 08:49:37 GMT")参数不同格式是否正常', () {
          expect(() => parseHttpDate('Sun,  06 Nov 1994 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06  Nov 1994 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov  1994 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov 1994  08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov 1994 08:49:37  GMT'),
              '');
        });

        test('parseHttpDate("Sun, 6 Nov 1994 08:49:37 GMT")不同格式是否正常', () {
          expect(() => parseHttpDate('Sun, 6 Nov 1994 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov 94 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov 1994 8:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov 1994 08:9:37 GMT'),
              '');

          expect(() => parseHttpDate('Sun, 06 Nov 1994 08:49:7 GMT'),
              '');
        });

        test('parseHttpDate("Sunday, 6 Nov 1994 08:49:37 GMT")', () {
          expect(() => parseHttpDate('Sunday, 6 Nov 1994 08:49:37 GMT'),
              '');
        });

        test('parseHttpDate("Sun, 6 November 1994 08:49:37 GMT")', () {
          expect(() => parseHttpDate('Sun, 6 November 1994 08:49:37 GMT'),
              '');
        });

        test('parseHttpDate("Sun, 6 Nov 1994 08:49:37 PST")', () {
          expect(() => parseHttpDate('Sun, 6 Nov 1994 08:49:37 PST'),
              '');
        });

        test('parseHttpDate("Sun, 6 Nov 1994 08:49:37 GMT ")', () {
          expect(() => parseHttpDate('Sun, 6 Nov 1994 08:49:37 GMT '),
              '');
        });
      });

      group('RFC 850', () {
        test('parseHttpDate("Sunday, 06-Nov-94 08:49:37 GMT")', () {
          final date = parseHttpDate('Sunday, 06-Nov-94 08:49:37 GMT');
          expect(date.day, (6));
          expect(date.month, (DateTime.november));
          expect(date.year, (1994));
          expect(date.hour, (8));
          expect(date.minute, (49));
          expect(date.second, (37));
          expect(date.timeZoneName, ('UTC'));
        });

        test('parseHttpDate("Sunday, 06-Nov-94 08:49:37 GMT")其他格式输出情况', () {
          expect(() => parseHttpDate('Sunday,06-Nov-94 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-9408:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94 08:49:37GMT'),
              '');

          expect(() => parseHttpDate('Sunday,  06-Nov-94 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94  08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94 08:49:37  GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 6-Nov-94 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-1994 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94 8:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94 08:9:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94 08:49:7 GMT'),
              '');
        });

        test('parseHttpDate("Sunday, 06-Nov-94 08:49:37 GMT")换成其他不存在的日期或时间', () {
          expect(() => parseHttpDate('Sunday, 00-Nov-94 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 31-Nov-94 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 32-Aug-94 08:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94 24:49:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94 08:60:37 GMT'),
              '');

          expect(() => parseHttpDate('Sunday, 06-Nov-94 08:49:60 GMT'),
              '');
        });

        test('parseHttpDate("Sun, 6-Nov-94 08:49:37 GMT")', () {
          expect(() => parseHttpDate('Sun, 6-Nov-94 08:49:37 GMT'),
              '');
        });

        test('parseHttpDate("Sunday, 6-November-94 08:49:37 GMT")', () {
          expect(() => parseHttpDate('Sunday, 6-November-94 08:49:37 GMT'),
              '');
        });

        test('parseHttpDate("Sunday, 6-Nov-94 08:49:37 PST")', () {
          expect(() => parseHttpDate('Sunday, 6-Nov-94 08:49:37 PST'),
              '');
        });

        test('parseHttpDate("Sunday, 6-Nov-94 08:49:37 GMT ")', () {
          expect(() => parseHttpDate('Sunday, 6-Nov-94 08:49:37 GMT '),
              '');
        });
      });

      group('asctime()', () {
        test('parseHttpDate("Sun Nov  6 08:49:37 1994")', () {
          final date = parseHttpDate('Sun Nov  6 08:49:37 1994');
          expect(date.day, (6));
          expect(date.month, (DateTime.november));
          expect(date.year, (1994));
          expect(date.hour, (8));
          expect(date.minute, (49));
          expect(date.second, (37));
          expect(date.timeZoneName, ('UTC'));
        });

        test('parseHttpDate("Sun Nov 16 08:49:37 1994")', () {
          final date = parseHttpDate('Sun Nov 16 08:49:37 1994');
          expect(date.day, (16));
          expect(date.month, (DateTime.november));
          expect(date.year, (1994));
          expect(date.hour, (8));
          expect(date.minute, (49));
          expect(date.second, (37));
          expect(date.timeZoneName, ('UTC'));
        });

        test('parseHttpDate("Sun Nov 16 08:49:37 1994")日期参数格式不一样的输出情况', () {
          expect(() => parseHttpDate('SunNov  6 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov6 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  608:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 08:49:371994'),
              '');

          expect(() => parseHttpDate('Sun  Nov  6 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov   6 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov 6 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6  08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 08:49:37  1994'),
              '');

          expect(() => parseHttpDate('Sun Nov 016 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 8:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 08:9:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 08:49:7 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 08:49:37 94'),
              '');

          expect(() => parseHttpDate('Sun Nov 0 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov 31 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Aug 32 08:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 24:49:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 08:60:37 1994'),
              '');

          expect(() => parseHttpDate('Sun Nov  6 08:49:60 1994'),
              '');
        });

        test('parseHttpDate("Sunday Nov 0 08:49:37 1994")', () {
          expect(() => parseHttpDate('Sunday Nov 0 08:49:37 1994'),
              '');
        });

        test('parseHttpDate("Sun November 0 08:49:37 1994")', () {
          expect(() => parseHttpDate('Sun November 0 08:49:37 1994'),
              '');
        });

        test('parseHttpDate("Sun November 0 08:49:37 1994 ")', () {
          expect(() => parseHttpDate('Sun November 0 08:49:37 1994 '),
              '');
        });
      });
    });
  }

}