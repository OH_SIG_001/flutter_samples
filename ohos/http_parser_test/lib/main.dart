import 'package:flutter/material.dart';
import 'package:http_parser_test/src/AuthenticationChallengeTestPage.dart';
import 'package:http_parser_test/src/CaseInsensitiveMapTestPage.dart';
import 'package:http_parser_test/src/ChunkedCodingTestPage.dart';
import 'package:http_parser_test/src/ExamplePage.dart';
import 'package:http_parser_test/src/HttpDateTestPage.dart';
import 'package:http_parser_test/src/MediaTypeTestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('authentication_challenge_test', AuthenticationChallengeTestPage('authentication_challenge_test')),
    MainItem('case_insensitive_map_test', CaseInsensitiveMapTestPage('case_insensitive_map_test')),
    MainItem('chunked_coding_test', ChunkedCodingTestPage('chunked_coding_test')),
    MainItem('example_test', ExamplePage('example_test')),
    MainItem('http_date_test', HttpDateTestPage('http_date_test')),
    MainItem('media_type_test', MediaTypeTestPage('media_type_test')),
  ];

  runApp(TestModelApp(
      appName: 'http_parser',
      data: app));
}
