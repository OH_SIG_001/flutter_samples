import 'dart:async';
import 'dart:math' show max;
import 'dart:typed_data';
import 'dart:ui';

import 'package:path_drawing/path_drawing.dart';

const List<String> renderingPaths = <String>[
  'M100,200 L3,4',
  'M100,200 l3,4',
  'M100,200 H3',
];
final Paint blackStrokePaint = Paint()
  ..color = const Color.fromARGB(255, 0, 0, 0)
  ..strokeWidth = 1.0
  ..style = PaintingStyle.stroke;
final Paint whiteFillPaint = Paint()
  ..color = const Color.fromARGB(255, 255, 255, 255)
  ..style = PaintingStyle.fill;

Future<Uint8List> getPathPngBytes(String pathString) async {
  final PictureRecorder rec = PictureRecorder();
  final Canvas canvas = Canvas(rec);

  final Path p = parseSvgPathData(pathString);

  final Rect bounds = p.getBounds();
  const double scaleFactor = 5.0;
  canvas.scale(scaleFactor);
  canvas.drawPaint(whiteFillPaint);

  canvas.drawPath(p, blackStrokePaint);

  final Picture pict = rec.endRecording();

  final int imgWidth =
      (max(bounds.width, bounds.right) * 2 * scaleFactor).ceil();
  final int imgHeight =
      (max(bounds.height, bounds.bottom) * 2 * scaleFactor).ceil();

  final Image image = await pict.toImage(imgWidth, imgHeight);
  final ByteData bytes = (await image.toByteData(format: ImageByteFormat.png))!;

  return bytes.buffer.asUint8List();
}