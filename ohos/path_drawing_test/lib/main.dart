import 'package:flutter/material.dart';
import 'package:path_drawing_test/src/DashPathTestPage.dart';
import 'package:path_drawing_test/src/ExamplePage.dart';
import 'package:path_drawing_test/src/RenderPathTestPage.dart';
import 'package:path_drawing_test/src/TrimPathTestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('example_test', const ExamplePage(title: 'example_test')),
    MainItem('dash_path_test', DashPathTestPage('dash_path_test')),
    MainItem('render_path_test', RenderPathTestPage('render_path_test')),
    MainItem('trim_path_test', TrimPathTestPage('trim_path_test')),
  ];

  runApp(TestModelApp(
      appName: 'path_drawing',
      data: app));
}