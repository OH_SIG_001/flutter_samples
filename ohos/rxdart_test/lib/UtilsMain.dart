import 'package:flutter/material.dart';
import 'package:rxdart_test/src/utils/CompositeSubscriptionTestPage.dart';

class UtilsMain extends StatefulWidget {
  UtilsMain(this.title);

  String title;

  @override
  State<UtilsMain> createState() => _UtilsMainMainState();

}

class _UtilsMainMainState extends State<UtilsMain> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          routeButton(context, 'composite_subscription_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => CompositeSubscriptionTestPage('composite_subscription_test')))),
        ],
      ),
    );
  }
}

Widget routeButton(BuildContext context, String title, Function() pageRoute) {
  return Container(
      margin: const EdgeInsets.only(top: 5, bottom: 5),
      height: 50,
      width: MediaQuery.of(context).size.width,
      color: Colors.grey[350],
      child: MaterialButton(
        onPressed: pageRoute,
        child: Text(title),
      )
  );
}