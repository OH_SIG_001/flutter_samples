import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../common/test_page.dart';

class ZipWithTestPage extends TestPage {
  ZipWithTestPage(super.title) {
    test('Rx.zipWith', () async {
      Stream<int>.value(1)
          .zipWith(Stream<int>.value(2), (int one, int two) => one + two)
          .listen((int result) {expect(result);});
    });

    test('Rx.zipWith accidental broadcast', () async {
      final controller = StreamController<int>();

      final stream =
      controller.stream.zipWith(Stream<int>.empty(), (_, dynamic __) => true);

      stream.listen(null);
      expect(() => stream.listen(null));

      controller.add(1);
    });

    test('Rx.zipWith on single stream should stay single ', () async {
      final delayedStream = Rx.timer(1, Duration(milliseconds: 10));
      final immediateStream = Stream.value(2);

      final concatenatedStream = delayedStream.zipWith(immediateStream, (a, int b) => a + b);

      expect(concatenatedStream.isBroadcast);
      expect(concatenatedStream);
    });

    test('Rx.zipWith on broadcast stream should stay broadcast ', () async {
      final delayedStream = Rx.timer(1, Duration(milliseconds: 10)).asBroadcastStream();
      final immediateStream = Stream.value(2);

      final concatenatedStream =
      delayedStream.zipWith(immediateStream, (a, int b) => a + b);

      expect(concatenatedStream.isBroadcast);
      expect(concatenatedStream);
    });

    test('Rx.zipWith multiple subscriptions on single ', () async {
      final delayedStream = Rx.timer(1, Duration(milliseconds: 10));
      final immediateStream = Stream.value(2);

      final concatenatedStream = delayedStream.zipWith(immediateStream, (a, int b) => a + b);

      expect(() => concatenatedStream.listen(null));
      expect(() => concatenatedStream.listen(null));
    });
  }

}