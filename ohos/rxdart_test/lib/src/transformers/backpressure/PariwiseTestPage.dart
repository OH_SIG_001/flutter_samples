import 'package:rxdart/rxdart.dart';

import '../../../common/test_page.dart';
import '../../utils.dart';

class PariwiseTestPage extends TestPage {
  PariwiseTestPage(super.title) {
    test('Rx.pairwise', () async {
      final stream = Rx.range(1, 4).pairwise();

      stream.listen(
        ((result) {
          expect(result);
        }),
        onError: ((Object e, StackTrace s) {}),
        onDone: (() {}),
      );

      expect(stream);

    });

    test('Rx.pairwise.empty', () {
      expect(Stream<int>.empty().pairwise());
    });

    test('Rx.pairwise.single', () {
      expect(Stream.value(1).pairwise());
    });

    test('Rx.pairwise.compatible', () {
      expect(
        Stream.fromIterable([1, 2]).pairwise(),
      );

      Stream<Iterable<int>> s = Stream.fromIterable([1, 2]).pairwise();
      expect(
        s,
      );
    });

    test('Rx.pairwise.asBroadcastStream', () async {
      final stream =
      Stream.fromIterable(const [1, 2, 3, 4]).asBroadcastStream().pairwise();

      // listen twice on same stream
      stream.listen(null);
      stream.listen(null);
      // code should reach here
      expect(true);
    });

    test('Rx.pairwise.error.shouldThrow.onError', () async {
      final streamWithError = Stream<void>.error(Exception()).pairwise();

      streamWithError.listen(null,
          onError: (Object e, StackTrace s) {
            expect(e);
          });
    });

    test('Rx.pairwise.nullable', () {
      nullableTest<Iterable<String?>>(
            (s) {
              expect(s.pairwise());
              return s.pairwise();
              },
      );
    });
  }

}