import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:math' as math;

void main() {
  runApp(
    // 使用MultiProvider包裹MyApp，使得MyApp下的子孙节点具备Provider的能力
    MultiProvider(
      providers: [
        ChangeNotifierProvider<BlockModel>(
            create: (context) => BlockModel()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PartRefresh Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'PartRefresh Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int buttonCount = 0;
  int firstCount = 0;
  int secondCount = 0;
  int randomCount = 0;

  Color selectColor(int index) {
    Color blockColor = Colors.white;
    switch(index){
      case 0:
        blockColor = Colors.tealAccent;
        break;
      case 1:
        blockColor = Colors.blue;
        break;
      case 2:
        blockColor = Colors.red;
        break;
      case 3:
        blockColor = Colors.green;
        break;
      case 4:
        blockColor = Colors.amber;
        break;
      case 5:
        blockColor = Colors.deepPurple;
        break;
      default:
        blockColor = Colors.white;
        break;
    }
    return blockColor;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('点击次数： $buttonCount'),
            const SizedBox(height: 10),
            const Text('first block: '),
            Consumer<BlockModel>(
                builder: (context, blockModel, child) {
                  return SizedBox(
                    height: 200,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(child: Container(
                          height: 100,
                          width: 100,
                          alignment: Alignment.center,
                          color: selectColor(blockModel.firstBlock),
                          child: Text('刷新次数： $firstCount'),
                        )),
                        Container(
                          alignment: Alignment.center,
                          height: 50,
                          width: 50,
                          child: MaterialButton(
                            onPressed: () {
                              buttonCount++;
                              firstCount++;
                              blockModel.changeFirstBlock();
                            },
                            child: const Icon(Icons.change_circle_rounded),
                          ),
                        ),
                      ],
                    ),
                  );
                }
            ),
            const Text('second block: '),
            Consumer<BlockModel>(
                builder: (context, blockModel, child) {
                  return SizedBox(
                    height: 200,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(child: Container(
                          height: 100,
                          width: 100,
                          alignment: Alignment.center,
                          color: selectColor(blockModel.secondBlock),
                          child: Text('刷新次数： $secondCount'),
                        )),
                        Container(
                          alignment: Alignment.center,
                          height: 50,
                          width: 50,
                          child: MaterialButton(
                            onPressed: () {
                              buttonCount++;
                              secondCount++;
                              blockModel.changeSecondBlock();
                            },
                            child: const Icon(Icons.change_circle_rounded),
                          ),
                        ),
                      ],
                    ),
                  );
                }
            ),
            SizedBox(
              height: 100,
              width: MediaQuery.of(context).size.width - 80,
              child: MaterialButton(
                color: Colors.blueGrey,
                onPressed: () {
                  buttonCount++;
                  randomCount++;
                  context.read<BlockModel>().refresh();
                },
                child: Text("Random modification: $randomCount"),
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: 100,
              width: MediaQuery.of(context).size.width - 80,
              child: MaterialButton(
                color: Colors.brown,
                onPressed: () {
                  setState(() {});
                },
                child: const Text("Refresh"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class BlockModel extends ChangeNotifier {
  int firstBlock = 0;
  int secondBlock = 0;

  void changeFirstBlock() {
    if(firstBlock < 5) {
      firstBlock++;
    } else {
      firstBlock = 0;
    }
    notifyListeners();
  }

  void changeSecondBlock() {
    if(secondBlock < 5) {
      secondBlock++;
    } else {
      secondBlock = 0;
    }
    notifyListeners();
  }

  void refresh() {
    firstBlock = math.Random().nextInt(5);
    secondBlock = math.Random().nextInt(5);
    notifyListeners();
  }
}