import 'dart:convert';

import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';

class MyEvent {
  var value;

  MyEvent(this.value);
}

class ExampleTestPage extends StatefulWidget {
  ExampleTestPage({required this.title});

  String title;

  @override
  State<StatefulWidget> createState() => _ExampleTestPageState();
}

class _ExampleTestPageState extends State<ExampleTestPage> {
  final EventBus eventBus = EventBus();

  String eventValue = '';

  @override
  void initState() {
    super.initState();
    eventBus.on().listen((event) {
      setState(() {
        eventValue =
            '${event.value.toString()}\n${event.value.runtimeType.toString()}';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                Text(
                  eventValue,
                  style: const TextStyle(fontSize: 30, color: Colors.black),
                ),
                ElevatedButton(
                  onPressed: () {
                    String value = 'Hello World!!!';
                    eventBus.fire(MyEvent(value));
                  },
                  child: const Text('Send String Type'),
                ),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    int value = 200;
                    eventBus.fire(MyEvent(value));
                  },
                  child: const Text('Send int Type'),
                ),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    Map<String, dynamic> value = {
                      'name': 'alan',
                      'age': 18,
                      'friends': ['张三', '李四']
                    };
                    eventBus.fire(MyEvent(value));
                  },
                  child: const Text('Send map Type'),
                ),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    List value = [0, 1, 2, 3];
                    eventBus.fire(MyEvent(value));
                  },
                  child: const Text('Send List Type'),
                ),
              ]))),
    );
  }
}
