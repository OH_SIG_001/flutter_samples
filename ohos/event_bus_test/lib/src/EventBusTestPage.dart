import 'package:event_bus/event_bus.dart';

import '../common/test_page.dart';

class EventA {
  String text;

  EventA(this.text);
}

class EventB {
  String text;

  EventB(this.text);
}

class EventWithMap {
  Map myMap;

  EventWithMap(this.myMap);
}

class EventBusTestPage extends TestPage {
  EventBusTestPage(super.title) {
    group('[EventBus]', () {
      test('EventBus().fire()发送一次信息, EventBus.on<EventA>', () {
        // given
        EventBus eventBus = EventBus();
        Future f = eventBus.on<EventA>().toList();

        // when
        eventBus.fire(EventA('a1'));
        eventBus.destroy();

        // then
        return f.then((events) {
          expect(events.length, 1);
        });
      });

      test('EventBus().fire()发送两次信息, EventBus.on<EventA>接收同一类型信息', () {
        // given
        EventBus eventBus = EventBus();
        Future f = eventBus.on<EventA>().toList();

        // when
        eventBus.fire(EventA('a1'));
        eventBus.fire(EventA('a2'));
        eventBus.destroy();

        // then
        return f.then((events) {
          expect(events.length, 2);
        });
      });

      test('EventBus().fire()发送两次信息, 分别生成EventBus.on<EventA>, EventBus.on<EventB>', () {
        // given
        EventBus eventBus = EventBus();
        Future f1 = eventBus.on<EventA>().toList();
        Future f2 = eventBus.on<EventB>().toList();

        // when
        eventBus.fire(EventA('a1'));
        eventBus.fire(EventB('b1'));
        eventBus.destroy();

        // then
        return Future.wait([
          f1.then((events) {
            expect(events.length, 1);
          }),
          f2.then((events) {
            expect(events.length, 1);
          })
        ]);
      });

      test('EventBus().fire()发送不同类型消息，不分类型同一接收', () {
        // given
        EventBus eventBus = EventBus();
        Future f = eventBus.on().toList();

        // when
        eventBus.fire(EventA('a1'));
        eventBus.fire(EventB('b1'));
        eventBus.fire(EventB('b2'));
        eventBus.destroy();

        // then
        return f.then((events) {
          expect(events.length, 3);
        });
      });

      test('以Map类型进行EventBus().fire()，并用Map类型进行接收', () {
        // given
        EventBus eventBus = EventBus();
        Future f = eventBus.on<EventWithMap>().toList();

        // when
        eventBus.fire(EventWithMap({'a': 'test'}));
        eventBus.destroy();

        // then
        return f.then((events) {
          expect(events.length, 1);
        });
      });
    });
  }

}