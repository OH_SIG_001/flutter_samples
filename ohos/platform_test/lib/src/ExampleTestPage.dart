import 'package:flutter/material.dart';
import 'package:platform/platform.dart';

class ExampleTestPage extends StatefulWidget {
  const ExampleTestPage({super.key, required this.title});

  final String title;

  @override
  State<ExampleTestPage> createState() => _ExampleTestPageState();
}

class _ExampleTestPageState extends State<ExampleTestPage> {
  LocalPlatform platform = LocalPlatform();

  String platformTitle = '';

  String value = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          const SizedBox(height: 10),
          Container(
            height: 150,
            child: ListView(
              children: [
                Text(
                  platformTitle,
                  style: Theme.of(context).textTheme.titleLarge,
                  textAlign: TextAlign.center,
                ),
                Text(
                  value,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          Expanded(
              child: ListView(
            children: [
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Operating System:';
                  value = platform.operatingSystem;
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.operatingSystem'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Number of Processors:';
                  value = platform.numberOfProcessors.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.numberOfProcessors'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Path Separator:';
                  value = platform.pathSeparator;
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.pathSeparator'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Local Hostname:';
                  value = platform.localHostname;
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.localHostname'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Environment:';
                  value = platform.environment.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.environment'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Executable:';
                  value = platform.executable;
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.executable'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Resolved Executable:';
                  value = platform.resolvedExecutable;
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.resolvedExecutable'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Script:';
                  value = platform.script.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.script'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Executable Arguments:';
                  value = platform.executableArguments.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.executableArguments'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Package Config:';
                  value = platform.packageConfig.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.packageConfig'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Version:';
                  value = platform.version;
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.version'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Stdin Supports ANSI:';
                  value = platform.stdinSupportsAnsi.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.operatingSystem'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Stdout Supports ANSI:';
                  value = platform.stdoutSupportsAnsi.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.stdoutSupportsAnsi'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'Locale Name:';
                  value = platform.localeName;
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.localeName'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'isAndroid:';
                  value = platform.isAndroid.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.isAndroid'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'isFuchsia:';
                  value = platform.isFuchsia.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.isFuchsia'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'isIOS:';
                  value = platform.isIOS.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.isIOS'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'isLinux:';
                  value = platform.isLinux.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.isLinux'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'isMacOS:';
                  value = platform.isMacOS.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.isMacOS'),
              ),
              const SizedBox(height: 10),
              MaterialButton(
                onPressed: () {
                  platformTitle = 'isWindows:';
                  value = platform.isWindows.toString();
                  setState(() {});
                },
                color: Colors.blue,
                child: const Text('platform.isWindows'),
              ),
              const SizedBox(height: 10),
            ],
          )),
        ],
      )),
    );
  }
}
