import 'dart:typed_data';

import 'package:uuid/uuid.dart';

import '../common/test_page.dart';


class UuidV1TestPage extends TestPage {
  var uuid = const Uuid();
  var testTime = 1321644961388;

  UuidV1TestPage(super.title) {
    group('[uuid.v1 Tests]', () {
      test('uuid.v1(options: {"mSecs": testTime})', () {
        expect(uuid.v1(options: {'mSecs': testTime}),
            uuid.v1(options: {'mSecs': testTime}));
      });

      test('uuid.v1(options: {"mSecs": testTime, "nSecs": 10000})', () {
        var thrown = false;
        try {
          uuid.v1(options: {'mSecs': testTime, 'nSecs': 10000});
        } catch (e) {
          thrown = true;
        }
        expect(thrown, true);
      });

      test('uuid.v1(options: {"mSecs": testTime}) - uuid.v1(options: {"mSecs": testTime - 1})', () {
        var uidt = uuid.v1(options: {'mSecs': testTime});
        var uidtb = uuid.v1(options: {'mSecs': testTime - 1});

        expect(
            (int.parse("0x${uidtb.split('-')[3]}") -
                int.parse("0x${uidt.split('-')[3]}")),
            '');
      });

      test('uuid.v1(options: {"mSecs": testTime, "nSecs": 10}) - uuid.v1(options: {"mSecs": testTime, "nSecs": 9})', () {
        var uidt = uuid.v1(options: {'mSecs': testTime, 'nSecs': 10});
        var uidtb = uuid.v1(options: {'mSecs': testTime, 'nSecs': 9});

        expect(
            (int.parse("0x${uidtb.split('-')[3]}") -
                int.parse("0x${uidt.split('-')[3]}")),
            1);
      });

      test('uuid.v1(options:{})', () {
        var id = uuid.v1(options: {
          'mSecs': 1321651533573,
          'nSecs': 5432,
          'clockSeq': 0x385c,
          'node': [0x61, 0xcd, 0x3c, 0xbb, 0x32, 0x10]
        });

        expect(id, 'd9428888-122b-11e1-b85c-61cd3cbb3210');
      });

      test('uuid.v1(options: {"mSecs": testTime, "nSecs": 9999}) - uuid.v1(options: {"mSecs": testTime + 1, "nSecs": 0})', () {
        var u0 = uuid.v1(options: {'mSecs': testTime, 'nSecs': 9999});
        var u1 = uuid.v1(options: {'mSecs': testTime + 1, 'nSecs': 0});

        var before = u0.split('-')[0], after = u1.split('-')[0];
        var dt = int.parse('0x$after') - int.parse('0x$before');

        expect(dt, 1);
      });

      test('uuids.contains(uuid.v1())', () {
        var uuids = <dynamic>{};
        var collisions = 0;
        for (var i = 0; i < 100; i++) {
          var code = uuid.v1();
          if (uuids.contains(code)) {
            collisions++;
            print('Collision of code: $code');
          } else {
            uuids.add(code);
          }
        }

        expect(collisions, 0);
        expect(uuids.length, 100);
      });

      test('Uuid().v1()[19]', () {
            for (var i = 0; i < 10; i++) {
              var code = Uuid().v1();
              expect(code[19], 'd');
              expect(code[19], 'c');
            }
          });

      test('uuid.v1buffer, Uuid.unparse(List<int> buffer, {int offset = 0})', () {
        var buffer = Uint8List(16);
        var options = {'mSecs': testTime, 'nSecs': 0};

        var wihoutBuffer = uuid.v1(options: options);
        uuid.v1buffer(buffer, options: options);

        expect('${Uuid.unparse(buffer)}, $wihoutBuffer', '');
      });

      test('uuid.v1({"mSecs": testTime, "nSecs": 0}), uuid.v1obj({"mSecs": testTime, "nSecs": 0})', () {
        var options = {'mSecs': testTime, 'nSecs': 0};

        var regular = uuid.v1(options: options);
        var obj = uuid.v1obj(options: options);

        expect('${obj.uuid}, $regular', '');
      });
    });
  }

}