import 'dart:typed_data';

import 'package:uuid/data.dart';
import 'package:uuid/uuid.dart';

import '../common/test_page.dart';

class UuidV6TestPage extends TestPage {
  var uuid = const Uuid();
  var testTime = 1321644961388;

  UuidV6TestPage(super.title) {
    group('[Version 6 Tests]', () {
      test('uuid.v6(config: V6Options(null, testTime, null, null, null))', () {
        expect(uuid.v6(config: V6Options(null, testTime, null, null, null)),
            uuid.v6(config: V6Options(null, testTime, null, null, null)));
      });

      test('uuid.v6(config: V6Options(null, testTime, 10000, null, null))', () {
        var thrown = false;
        try {
          uuid.v6(config: V6Options(null, testTime, 10000, null, null));
        } catch (e) {
          thrown = true;
        }
        expect(thrown, (true));
      });

      test('uuid.v6(config: V6Options(null, testTime, null, null, null)) - uuid.v6(config: V6Options(null, testTime - 1, null, null, null))', () {
        var uidt = uuid.v6(config: V6Options(null, testTime, null, null, null));
        var uidtb = uuid.v6(config: V6Options(null, testTime - 1, null, null, null));

        expect(
            (int.parse("0x${uidtb.split('-')[3]}") -
                int.parse("0x${uidt.split('-')[3]}")),
            '');
      });

      test('uuid.v6(config: V6Options(null, testTime, 10, null, null)) - uuid.v6(config: V6Options(null, testTime, 9, null, null))', () {
        var uidt = uuid.v6(config: V6Options(null, testTime, 10, null, null));
        var uidtb = uuid.v6(config: V6Options(null, testTime, 9, null, null));

        expect(
            (int.parse("0x${uidtb.split('-')[3]}") -
                int.parse("0x${uidt.split('-')[3]}")),
            1);
      });

      test('uuid.v6(config: V6Options(0x385c, 1321651533573, 5432, [0x61, 0xcd, 0x3c, 0xbb, 0x32, 0x10], null)', () {
        var id = uuid.v6(
            config: V6Options(0x385c, 1321651533573, 5432,
                [0x61, 0xcd, 0x3c, 0xbb, 0x32, 0x10], null));

        expect(id, '1e1122bd-9428-6888-b85c-61cd3cbb3210');
      });

      test('uuid.v6(config: V6Options(null, testTime, 9999, null, null)).split("-")[2] - uuid.v6(config: V6Options(null, testTime + 1, 0, null, null)).split("-")[2]', () {
        var u0 = uuid.v6(config: V6Options(null, testTime, 9999, null, null));
        var u1 = uuid.v6(config: V6Options(null, testTime + 1, 0, null, null));

        var before = u0.split('-')[2], after = u1.split('-')[2];
        var dt = int.parse('0x$after') - int.parse('0x$before');

        expect(dt, 1);
      });

      test('生成多个Uuid().v6()以查看是否发生v6冲突', () {
        var uuids = <dynamic>{};
        var collisions = 0;
        for (var i = 0; i < 100; i++) {
          var code = uuid.v6();
          if (uuids.contains(code)) {
            collisions++;
            print('Collision of code: $code');
          } else {
            uuids.add(code);
          }
        }

        expect(collisions, 0);
        expect(uuids.length, 100);
      });

      test('生成多个Uuid().v6()以检查是否有变体v6', () {
            for (var i = 0; i < 100; i++) {
              var code = Uuid().v6();
              expect(code[19], 'd');
              expect(code[19], 'c');
            }
          });

      test('uuid.v6buffer(buffer, config: options), Uuid.unparse(buffer), uuid.v6(config: options)', () {
        var buffer = Uint8List(16);
        var options = V6Options(null, testTime, 0, null, null);

        var wihoutBuffer = uuid.v6(config: options);
        uuid.v6buffer(buffer, config: options);

        expect('${Uuid.unparse(buffer)}, $wihoutBuffer', '');
      });

      test('uuid.v6(config: options), uuid.v6obj(config: options)', () {
        var options = V6Options(null, testTime, 0, null, null);

        var regular = uuid.v6(config: options);
        var obj = uuid.v6obj(config: options);

        expect('${obj.uuid}, $regular', '');
      });
    });
  }

}