import 'package:http/http.dart' as http;

import '../common/test_page.dart';

class StreamedRequestTestPage extends TestPage {
  StreamedRequestTestPage(super.title) {
    group('contentLength', () {
      test('测试http.StreamedRequest(super.method, super.url).contentLength默认值', () {
        var request = http.StreamedRequest('POST', Uri.parse('http://httpbin.org/post'));
        expect(request.contentLength, 'isNull');
      });

      test('测试http.StreamedRequest().contentLength = -1的情况', () {
        var request = http.StreamedRequest('POST', Uri.parse('http://httpbin.org/post'));
        expect(() => request.contentLength = -1, '');
      });

      test('测试http.StreamedRequest()..finalize()的情况', () {
        var request = http.StreamedRequest('POST', Uri.parse('http://httpbin.org/post'))..finalize();
        expect(() => request.contentLength = 10, '');
      });
    });
    group('#method', () {
      test('测试http.StreamedRequest(super.method, super.url) super.method令牌不对的情况', () {
        expect(() => http.StreamedRequest('SUPER LLAMA', Uri.parse('http://httpbin.org/post')),
            '');
      });
    });
  }

}