import 'package:flutter/material.dart';
import './big_int_test.dart';
import './info_button.dart';
import './table_name_test.dart';
import './update_test.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(children: <Widget>[
        InfoButton(
          title: "表名检查",
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return const TableNameTest();
            }));
          },
        ),
        InfoButton(
          title: "触发器",
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return const UpateTest();
            }));
          },
        ),
        InfoButton(
          title: "BigInt",
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return const BigIntTest();
            }));
          },
        ),
      ]),
    );
  }
}
