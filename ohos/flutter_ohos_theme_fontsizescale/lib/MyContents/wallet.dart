import 'package:flutter/material.dart';

class WalletView extends StatefulWidget{
  const WalletView({super.key});

  @override
  WalletViewState createState() => WalletViewState();
}
class WalletViewState extends State<WalletView>{
  @override
  Widget build(BuildContext context) {

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;

    return MaterialApp(
      theme: ThemeData(
        primaryColor: sysColorMode == Brightness.dark? Colors.white : Colors.black,
        brightness: sysColorMode
      ),
      home: Scaffold(
        // appBar: AppBar(title: const Text("我"),backgroundColor: Colors.grey[500],),
        body: _walletListView()
      )
    );
  }

  ListView _walletListView() {
    return ListView(
        children: <Widget>[
          Container(
            height: 130.0,
            // color: Colors.grey[500],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  //首付款
                  mainAxisAlignment: MainAxisAlignment.center,
                  //首付款
                  children: const <Widget>[
                    Icon(Icons.check_box),
                    Text(" "),
                    Text("首付款"),
                  ],
                ),
                Column(
                  //零钱
                  mainAxisAlignment: MainAxisAlignment.center,
                  //零钱
                  children: const <Widget>[
                    Icon(Icons.monetization_on),
                    Text(" "),
                    Text("零钱"),
                    Text("6896.26"),
                  ],
                ),
                Column(
                  //银行卡
                  mainAxisAlignment: MainAxisAlignment.center,
                  //银行卡
                  children: const <Widget>[
                    Icon(Icons.credit_card),
                    Text(" "),
                    Text("银行卡"),
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 40.0,
            // color: Colors.grey[100],
            padding: const EdgeInsets.only(left: 15.0,top: 10.0),
            child: const Text("我的服务"),
          ),
          SizedBox(
            height: 90.0,
            child: Row(
              //信用卡还款,手机充值，理财通
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.credit_card),
                    Text("信用卡还款"),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.phone_iphone),
                    Text("手机充值"),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.fiber_smart_record),
                    Text("理财专区"),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 90.0,
            child: Row(
              //生活缴费，城市服务
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.edit_location),
                    Text("生活缴费",textAlign: TextAlign.center,),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.spa),
                    Text("账号充值",textAlign: TextAlign.center,),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.location_city),
                    Text("城市服务"),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 90.0,
            child: Row(
              //我的商城，我的公益，保险服务
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.local_pizza),
                    Text("我的商城"),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.favorite),
                    Text("我的公益"),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.beenhere),
                    Text("保险服务"),
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 40.0,
            // color: Colors.grey[100],
            padding: const EdgeInsets.only(left: 15.0,top: 10.0),
            child: const Text("限时推广"),
          ),
          Container(
            height: 90.0,
            padding: const EdgeInsets.only(left: 40.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.sim_card),
                    Text("SIM卡"),
                  ],
                ),
              ],
            ),
          ),
        ],
      );
  }
}