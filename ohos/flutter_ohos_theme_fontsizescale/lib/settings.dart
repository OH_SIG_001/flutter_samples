import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget{
  const SettingsPage({super.key});

  @override
  SettingsViewState createState() => SettingsViewState();
}
class SettingsViewState extends State{
  @override
  Widget build(BuildContext context) {

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;

    return MaterialApp(
      title: "设置",
      theme: ThemeData(
        // primaryColor: Colors.blue,
        primaryColor: sysColorMode == Brightness.dark? Colors.white : Colors.black,
        brightness: sysColorMode  // system theme settings
      ),
      home: Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: const ListTile(
                // leading: Icon(Icons.dashboard),
                title: Text("账号与安全"),
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: const ListTile(
                // leading: Icon(Icons.dashboard),
                title: Text("青少年模式"),
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: const ListTile(
                // leading: Icon(Icons.dashboard),
                title: Text("关怀模式"),
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: const ListTile(
                // leading: Icon(Icons.dashboard),
                title: Text("新消息通知"),
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: const ListTile(
                // leading: Icon(Icons.dashboard),
                title: Text("聊天"),
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: ListTile(
                // leading: Icon(Icons.dashboard),
                title: const Text("通用（可点击）"),
                onTap: () {
                     Navigator.push(context, MaterialPageRoute(builder: (context) => CommonPage()));
                },
              ),
            ),
          ),
        ],
      )
    )
    );
  }
}


class CommonPage extends StatelessWidget {
  const CommonPage({super.key});

  @override
  Widget build(BuildContext context) {

    Brightness  sysColorMode = MediaQuery.of(context).platformBrightness;

    return MaterialApp(
      title: "通用",
      theme: ThemeData(
        // primaryColor: Colors.blue,
        brightness: sysColorMode,
      ),
      home: Scaffold(
          appBar: AppBar(title: const Text("通用设置")),
          body: ListView(
            children: <Widget>[
              Container(
                padding:const EdgeInsets.only(top: 20.0),
                // color: Colors.grey[200],
                child: Container(
                // color: Colors.white,
                height: 50.0,
                 child: Container(
                  // color: Colors.white,
                  height: 50.0,
                  child: ListTile(
                    leading: const Icon(Icons.settings),
                    title: const Text("应用主题（可点击）"),
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => DarkModePage()));
                    },
                ),
                ),
                ),
              ),
              Container(
                padding:const EdgeInsets.only(top: 20.0),
                // color: Colors.grey[200],
                // ignore: sized_box_for_whitespace
                child: Container(
                // color: Colors.white,
                height: 50.0,
                 // ignore: sized_box_for_whitespace
                 child: Container(
                  // color: Colors.white,
                  height: 50.0,
                  child: ListTile(
                    leading: const Icon(Icons.settings),
                    title: const Text("字体大小（可点击）"),
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => FontSizePage()));
                    },
                ),
                ),
                ),
              )
            ],
          )
      ),
    );
  }
}

class FontSizePage extends StatelessWidget {
  const FontSizePage({super.key});

  @override
  Widget build(BuildContext context) {

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;
    print("******* 当前系统主题为：$sysColorMode **********");
    double sysTextScaleFactor = MediaQuery.of(context).textScaleFactor;
    print("=============== 当前系统字体大小系数：$sysTextScaleFactor ============");

    return MaterialApp(
      title: "字体大小",
      theme: ThemeData(
        // primaryColor: Colors.blue,
        // textTheme: TextTheme(
        //   // ignore: deprecated_member_use
        //   bodyText2: TextStyle(
        //     fontSize: 12 * sysTextScaleFactor
        //   ), 
        // ),
        brightness: sysColorMode  
      ),
      home: Scaffold(
          appBar: AppBar(title: const Text("字体大小")),
          body: ListView(
            children: <Widget>[
              Container(
                padding:const EdgeInsets.only(top: 20.0),
                // color: Colors.grey[200],
                // ignore: sized_box_for_whitespace
                child: Container(
                // color: Colors.white,
                height: 50.0,
                child: const ListTile(
                // leading: Icon(Icons.dashboard),
                title: Text("星河璀璨，鸿蒙正当时 (字体大小固定 ×1.0倍)", textScaleFactor: 1.0,),
                ),
              ),
            ),
            Container(
                padding:const EdgeInsets.only(top: 20.0),
                // color: Colors.grey[200],
                // ignore: sized_box_for_whitespace
                child: Container(
                // color: Colors.white,
                height: 50.0,
                child: ListTile(
                // leading: Icon(Icons.dashboard),
                title: Text("星河璀璨，鸿蒙正当时 (字体大小跟随系统改变 ×$sysTextScaleFactor倍)"),
                ),
              ),
            ),
            ],
          )
          
      ),
    );
  }

}

class DarkModePage extends StatelessWidget {
  const DarkModePage({super.key});

  @override
  Widget build(BuildContext context) {

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;

    return MaterialApp(
      title: "深色模式",
      theme: ThemeData(
        primaryColor: sysColorMode == Brightness.dark? Colors.white : Colors.black,
        brightness: sysColorMode  
      ),
      home: Scaffold(
          appBar: AppBar(title: const Text("应用主题")),
          body: ListView(
            children: <Widget>[
              Container(
                padding:const EdgeInsets.only(top: 20.0),
                // color: Colors.grey[200],
                // ignore: sized_box_for_whitespace
                child: Container(
                // color: Colors.white,
                height: 50.0,
                child:  ListTile(
                // leading: Icon(Icons.dashboard),
                title: Text("当前主题：$sysColorMode"),
                ),
              ),
            ),
            ],
          )
          
      ),
    );
  }

}