import 'package:flutter/material.dart';

class MessageView extends StatefulWidget{
  const MessageView({super.key});

  @override
  // ignore: library_private_types_in_public_api
  MessageViewState createState() => MessageViewState();
}
class MessageViewState extends State{

  @override
  Widget build(BuildContext context) {

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;
    double sysTextScaleFactor = MediaQuery.of(context).textScaleFactor;
   
    return MaterialApp(
      
      theme: ThemeData(
        primaryColor: sysColorMode == Brightness.dark? Colors.white : Colors.black,
        brightness: sysColorMode,
      ),
      home: Scaffold(
        body: _buildListView(sysColorMode),
      )
    );
  }
  ListView _buildListView(sysColorMode){
    return ListView(
      children: <Widget>[
        ListTile(
          onTap: (){
            setState(() {
              // ignore: avoid_print
              print("---当前系统字体系数：${MediaQuery.of(context).textScaleFactor}");  // 更新状态变量 
              // ignore: avoid_print
              print("---当前系统主题颜色：${MediaQuery.of(context).platformBrightness}");  // 更新状态变量 
            });
          },
          leading: (sysColorMode == Brightness.light)? 
          Image.asset("images/img.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/img.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
          title: const Text("Dragon"),
          subtitle: const Text("这个需求尽快解决一下"),
          trailing: const Text("上午 9:10"),
        ),
        ListTile(
          leading: (sysColorMode == Brightness.light)? 
          Image.asset("images/a006.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a006.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
          title: const Text("Cattle"),
          subtitle: const Text("hello world！"),
          trailing: const Text("上午 11:02"),
        ),
        ListTile(
         leading: (sysColorMode == Brightness.light)? 
          Image.asset("images/xk.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/xk.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
          title: const Text("Mouse"),
          subtitle: const Text("交付压力还是很大的"),
          trailing: const Text("下午 6:30"),
        ),
        ListTile(
          leading: (sysColorMode == Brightness.light)? 
          Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
          title: const Text("Rabbit"),
          subtitle: const Text("您好，bug处理下"),
          trailing: const Text("昨天"),
        ),
        ListTile(
         leading: (sysColorMode == Brightness.light)? 
          Image.asset("images/a003.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a003.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
          title: const Text("Tiger"),
          subtitle: const Text("bye bye"),
          trailing: const Text("昨天"),
        ),
        ListTile(
          leading: (sysColorMode == Brightness.light)? 
          Image.asset("images/a002.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a002.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
          title: const Text("Dog"),
          subtitle: const Text("打球不"),
          trailing: const Text("8月1日"),
        ),
      ],
    );
  }
}