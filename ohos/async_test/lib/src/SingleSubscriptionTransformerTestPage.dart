import 'dart:async';

import 'package:async/async.dart';
import 'package:async_test/src/utils.dart';

import '../common/test_page.dart';

class SingleSubscriptionTransformerTestPage extends TestPage {
  SingleSubscriptionTransformerTestPage(super.title) {
    test("SingleSubscriptionTransformer()绑定后立即缓冲事件", () async {
      var controller = StreamController.broadcast();
      var stream = controller.stream.transform(const SingleSubscriptionTransformer());

      controller.add(1);
      controller.add(2);
      await flushMicrotasks();

      expect(stream.toList());
      await flushMicrotasks();

      controller.add(3);
      controller.add(4);
      controller.close();
    });

    test("SingleSubscriptionTransformer()取消订阅广播流时取消订阅", () async {
          var canceled = false;
          var controller = StreamController.broadcast(onCancel: () {
            canceled = true;
          });
          var stream = controller.stream.transform(const SingleSubscriptionTransformer());
          await flushMicrotasks();
          expect(canceled);

          var subscription = stream.listen(null);
          await flushMicrotasks();
          expect(canceled);

          subscription.cancel();
          await flushMicrotasks();
          expect(canceled);
        });
  }

}