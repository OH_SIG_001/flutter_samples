import 'dart:async';

import 'package:async/async.dart';

import '../common/test_page.dart';

class StreamExtensionsTestPage extends TestPage {
  StreamExtensionsTestPage(super.title) {
    group('.slices', () {
      test('.slices() 空的', () {
        expect(Stream.empty().slices(1).toList());
      });

      test('长度与可迭代项相同', () {
        expect(
            Stream.fromIterable([1, 2, 3]).slices(3).toList()
        );
      });

      test('长度比可迭代长度长', () {
        expect(
            Stream.fromIterable([1, 2, 3]).slices(5).toList()
        );
      });

      test('长度比可迭代的短', () {
        expect(
            Stream.fromIterable([1, 2, 3]).slices(2).toList()
        );
      });

      test('长度可被可迭代项整除', () {
        expect(
            Stream.fromIterable([1, 2, 3, 4]).slices(2).toList()
        );
      });

      test('拒绝负长度', () {
        expect(() => Stream.fromIterable([1]).slices(-1));
      });

      test('拒绝长度0', () {
        expect(() => Stream.fromIterable([1]).slices(0));
      });
    });

    group('.firstOrNull', () {
      test('.firstOrNull 返回第一个数据事件', () {
        expect(Stream.fromIterable([1, 2, 3, 4]).firstOrNull);
      });

      test('返回第一个错误事件', () {
        expect(Stream.error('oh no').firstOrNull);
      });

      test('为空流返回null', () {
        expect(Stream.empty().firstOrNull);
      });

      test('事件后取消订阅', () async {
        var isCancelled = false;
        var controller = StreamController<int>(onCancel: () {
          isCancelled = true;
        });
        controller.add(1);

        expect(controller.stream.firstOrNull);
        expect(isCancelled);
      });

      test('出现错误后取消订阅', () async {
        var isCancelled = false;
        var controller = StreamController<int>(onCancel: () {
          isCancelled = true;
        });
        controller.addError('oh no');

        expect(controller.stream.firstOrNull);
        expect(isCancelled);
      });
    });

    //  版本不支持
    // group('.listenAndBuffer', () {
    //   test('发出在侦听listenAndBuffer之前添加的事件', () async {
    //     var controller = StreamController<int>()
    //       ..add(1)
    //       ..add(2)
    //       ..add(3)
    //       ..close();
    //     var stream = controller.stream.listenAndBuffer();
    //     await pumpEventQueue();
    //
    //     expect(stream, [1, 2, 3, '']);
    //   });
    //
    //   test('发出在侦听listenAndBuffer之后添加的事件', () async {
    //     var controller = StreamController<int>();
    //     var stream = controller.stream.listenAndBuffer();
    //     expect(stream, [1, 2, 3, '']);
    //     await pumpEventQueue();
    //
    //     controller
    //       ..add(1)
    //       ..add(2)
    //       ..add(3)
    //       ..close();
    //   });
    //
    //   test('发出在侦听listenAndBuffer之前和之后添加的事件',
    //           () async {
    //         var controller = StreamController<int>()
    //           ..add(1)
    //           ..add(2)
    //           ..add(3);
    //         var stream = controller.stream.listenAndBuffer();
    //         expect(stream, [1, 2, 3, 4, 5, 6, '']);
    //         await pumpEventQueue();
    //
    //         controller
    //           ..add(4)
    //           ..add(5)
    //           ..add(6)
    //           ..close();
    //       });
    //
    //   test('调用listenAndBuffer()后立即侦听', () async {
    //     var listened = false;
    //     var controller = StreamController<int>(onListen: () {
    //       listened = true;
    //     });
    //     controller.stream.listenAndBuffer();
    //     expect(listened, true);
    //   });
    //
    //   test('转发暂停和恢复', () async {
    //     var controller = StreamController<int>();
    //     var stream = controller.stream.listenAndBuffer();
    //     expect(controller.isPaused, false);
    //     var subscription = stream.listen(null);
    //     expect(controller.isPaused, false);
    //     subscription.pause();
    //     expect(controller.isPaused, true);
    //     subscription.resume();
    //     expect(controller.isPaused, false);
    //   });
    //
    //   test('远期取消', () async {
    //     var completer = Completer<void>();
    //     var canceled = false;
    //     var controller = StreamController<int>(onCancel: () {
    //       canceled = true;
    //       return completer.future;
    //     });
    //     var stream = controller.stream.listenAndBuffer();
    //     expect(canceled, false);
    //     var subscription = stream.listen(null);
    //     expect(canceled, false);
    //
    //     var cancelCompleted = false;
    //     subscription.cancel().then((_) {
    //       cancelCompleted = true;
    //     });
    //     expect(canceled, true);
    //     await pumpEventQueue();
    //     expect(cancelCompleted, false);
    //
    //     completer.complete();
    //     await pumpEventQueue();
    //     expect(cancelCompleted, true);
    //   });
    // });
  }

}