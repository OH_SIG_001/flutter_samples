import { ArrayList } from '@kit.ArkTS';
import { Log } from '@ohos/flutter_ohos';

export interface DataModelObserver {
  onCountUpdate(newCount: number): void;
}

export class DataModel {
  public static instance = new DataModel();
  private counter = 0;
  private observers: ArrayList<DataModelObserver> = new ArrayList();

  private constructor() {
  }

  public increase() {
    this.setCounter(++this.counter);
  }

  public setCounter(newCounter: number): void {
    Log.i("Multi->setCounter", "newCounter=" + newCounter);
    this.observers.forEach(observer => {
      observer?.onCountUpdate(this.counter);
    });
    this.counter = newCounter;
  }

  public getCounter(): number {
    return this.counter;
  }

  addObserver(observer: DataModelObserver): void {
    this.observers.add(observer);
  }

  removeObserver(observer: DataModelObserver): boolean {
    return this.observers.remove(observer);
  }
}