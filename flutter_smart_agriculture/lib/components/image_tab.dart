import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/pages/detail_page.dart';

import '../models/base_type_model.dart';

typedef ValueChanged<T> = void Function(T value);

//图片列表菜单
class ImageTab extends StatefulWidget {
  final BaseTypeInfo info;

  const ImageTab(this.info, {super.key});

  @override
  State<StatefulWidget> createState() => _ImageTab();
}

class _ImageTab extends State<ImageTab> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, bottom: 8),
      child: GestureDetector(
        child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0), // 设置圆角半径
          ),
          child: SizedBox(
            width: double.infinity,
            child: Column(
              children: [
                AspectRatio(
                  aspectRatio: 16 / 9,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(16.0),
                        topRight: Radius.circular(16.0)),
                    child:
                        Image.asset(widget.info.imageAsset, fit: BoxFit.cover),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 10, bottom: 18),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.info.title,
                        style: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "预计收获期：${widget.info.harvestingDate ?? ""}",
                        style: const TextStyle(
                            fontSize: 14,
                            color: Colors.grey,
                            fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => DetailPage(widget.info)));
        },
      ),
    );
  }
}
