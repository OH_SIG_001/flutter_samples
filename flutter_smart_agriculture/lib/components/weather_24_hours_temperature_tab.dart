import 'package:flutter/material.dart';

import '../models/weather_24_hours_model.dart';

class Weather24HoursTemperatureTab extends StatefulWidget {
  const Weather24HoursTemperatureTab({super.key});

  @override
  State<StatefulWidget> createState() => _Weather24HoursTemperatureTabState();
}

class _Weather24HoursTemperatureTabState
    extends State<Weather24HoursTemperatureTab> {
  List<HourWeatherInfo>? list;

  @override
  void initState() {
    super.initState();
    getHourWeatherInfo().then((value) => setState(() {
          list = value;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 15),
            child: Text(
              "24小时天气预报",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
          list == null
              ? const Text("加载中...")
              : SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10, top: 8),
                    child: Row(
                      children: list!.map((hourInfo) {
                        return Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Column(
                            children: [
                              Text(
                                hourInfo.timeDesc ?? "",
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 12),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 8.0, top: 8.0),
                                child: Image.asset(
                                  hourInfo.weatherAssets ?? "",
                                  width: 28,
                                  height: 28,
                                ),
                              ),
                              Text(hourInfo.temp ?? "",
                                  style: const TextStyle(
                                      color: Colors.white, fontSize: 12)),
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                )
        ],
      ),
    );
  }
}
