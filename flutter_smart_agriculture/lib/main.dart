import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/components/FpsTextComponent.dart';
import 'pages/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "智慧农业",
        theme: ThemeData(primaryColor: Colors.blue),
        home: Scaffold(
          body: const HomePage(),
          // floatingActionButton: Container(
          //   margin: const EdgeInsets.only(top: 0, left: 0),
          //   width: 76,
          //   height: 45,
          //   child: FloatingActionButton(
          //     onPressed: () {
          //       // Add your onPressed code here!
          //     },
          //     backgroundColor: const Color(0x44ff0000),
          //     shape: RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(6.0)),
          //     child: const FpsTextComponent(),
          //   ),
          //
          // ),
          // floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        ));
  }
}
