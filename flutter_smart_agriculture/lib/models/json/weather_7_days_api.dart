import 'package:openvalley_smart_agriculture/models/json/base_api.dart';

class Weather7DaysApi extends BaseApi{
  String? code;
  String? updateTime;
  String? fxLink;
  List<Daily>? daily;
  Refer? refer;

  Weather7DaysApi(
      {this.code, this.updateTime, this.fxLink, this.daily, this.refer});

  Weather7DaysApi.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    updateTime = json['updateTime'];
    fxLink = json['fxLink'];
    if (json['daily'] != null) {
      daily = <Daily>[];
      json['daily'].forEach((v) {
        daily!.add(new Daily.fromJson(v));
      });
    }
    refer = json['refer'] != null ? new Refer.fromJson(json['refer']) : null;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['updateTime'] = this.updateTime;
    data['fxLink'] = this.fxLink;
    if (this.daily != null) {
      data['daily'] = this.daily!.map((v) => v.toJson()).toList();
    }
    if (this.refer != null) {
      data['refer'] = this.refer!.toJson();
    }
    return data;
  }
}

class Daily {
  String? fxDate;
  String? sunrise;
  String? sunset;
  String? moonrise;
  String? moonset;
  String? moonPhase;
  String? moonPhaseIcon;
  String? tempMax;
  String? tempMin;
  String? iconDay;
  String? textDay;
  String? iconNight;
  String? textNight;
  String? wind360Day;
  String? windDirDay;
  String? windScaleDay;
  String? windSpeedDay;
  String? wind360Night;
  String? windDirNight;
  String? windScaleNight;
  String? windSpeedNight;
  String? humidity;
  String? precip;
  String? pressure;
  String? vis;
  String? cloud;
  String? uvIndex;

  Daily(
      {this.fxDate,
      this.sunrise,
      this.sunset,
      this.moonrise,
      this.moonset,
      this.moonPhase,
      this.moonPhaseIcon,
      this.tempMax,
      this.tempMin,
      this.iconDay,
      this.textDay,
      this.iconNight,
      this.textNight,
      this.wind360Day,
      this.windDirDay,
      this.windScaleDay,
      this.windSpeedDay,
      this.wind360Night,
      this.windDirNight,
      this.windScaleNight,
      this.windSpeedNight,
      this.humidity,
      this.precip,
      this.pressure,
      this.vis,
      this.cloud,
      this.uvIndex});

  Daily.fromJson(Map<String, dynamic> json) {
    fxDate = json['fxDate'];
    sunrise = json['sunrise'];
    sunset = json['sunset'];
    moonrise = json['moonrise'];
    moonset = json['moonset'];
    moonPhase = json['moonPhase'];
    moonPhaseIcon = json['moonPhaseIcon'];
    tempMax = json['tempMax'];
    tempMin = json['tempMin'];
    iconDay = json['iconDay'];
    textDay = json['textDay'];
    iconNight = json['iconNight'];
    textNight = json['textNight'];
    wind360Day = json['wind360Day'];
    windDirDay = json['windDirDay'];
    windScaleDay = json['windScaleDay'];
    windSpeedDay = json['windSpeedDay'];
    wind360Night = json['wind360Night'];
    windDirNight = json['windDirNight'];
    windScaleNight = json['windScaleNight'];
    windSpeedNight = json['windSpeedNight'];
    humidity = json['humidity'];
    precip = json['precip'];
    pressure = json['pressure'];
    vis = json['vis'];
    cloud = json['cloud'];
    uvIndex = json['uvIndex'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fxDate'] = this.fxDate;
    data['sunrise'] = this.sunrise;
    data['sunset'] = this.sunset;
    data['moonrise'] = this.moonrise;
    data['moonset'] = this.moonset;
    data['moonPhase'] = this.moonPhase;
    data['moonPhaseIcon'] = this.moonPhaseIcon;
    data['tempMax'] = this.tempMax;
    data['tempMin'] = this.tempMin;
    data['iconDay'] = this.iconDay;
    data['textDay'] = this.textDay;
    data['iconNight'] = this.iconNight;
    data['textNight'] = this.textNight;
    data['wind360Day'] = this.wind360Day;
    data['windDirDay'] = this.windDirDay;
    data['windScaleDay'] = this.windScaleDay;
    data['windSpeedDay'] = this.windSpeedDay;
    data['wind360Night'] = this.wind360Night;
    data['windDirNight'] = this.windDirNight;
    data['windScaleNight'] = this.windScaleNight;
    data['windSpeedNight'] = this.windSpeedNight;
    data['humidity'] = this.humidity;
    data['precip'] = this.precip;
    data['pressure'] = this.pressure;
    data['vis'] = this.vis;
    data['cloud'] = this.cloud;
    data['uvIndex'] = this.uvIndex;
    return data;
  }
}

class Refer {
  List<String>? sources;
  List<String>? license;

  Refer({this.sources, this.license});

  Refer.fromJson(Map<String, dynamic> json) {
    sources = json['sources'].cast<String>();
    license = json['license'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sources'] = this.sources;
    data['license'] = this.license;
    return data;
  }
}
