import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/components/loading_failed.dart';
import 'package:openvalley_smart_agriculture/components/weather_7_days_tab.dart';

import '../components/loading_page.dart';
import '../components/weather_24_hours_temperature_tab.dart';
import '../models/weather_24_hours_model.dart';
import '../models/weather_7_days_model.dart';

class WeatherPage extends StatefulWidget {
  const WeatherPage({super.key});

  @override
  State<StatefulWidget> createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  DayWeatherInfo? dayWeatherInfo;
  HourWeatherInfo? nowWeatherInfo;

  bool loading = true;
  bool loadingError = false;
  bool _hasDisposed = false;

  void setError(Object? err) {
    if (err != null) {
      print(err);
    }
    setState(() {
      loadingError = true;
      loading = false;
    });
  }

  void finishLoading() {
    if (_hasDisposed) {
      return;
    }
    setState(() {
      loading = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _hasDisposed = true;
  }

  @override
  void initState() {
    super.initState();
    getDayWeatherList()
        .then((value) => setState(() {
              dayWeatherInfo = value[0];
              if (dayWeatherInfo == null) {
                setError(null);
              } else if (nowWeatherInfo != null) {
                finishLoading();
              }
            }))
        .catchError((err) {
      setError(err);
    });
    getHourWeatherInfo()
        .then((value) => setState(() {
              nowWeatherInfo = value[0];
              if (nowWeatherInfo == null) {
                setError(null);
              } else if (dayWeatherInfo != null) {
                finishLoading();
              }
            }))
        .catchError((err) {
      setError(err);
    });
    Future.delayed(const Duration(seconds: 5), () {
      //5秒后超时
      finishLoading();
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? const LoadingPage()
        : loadingError
            ? const LoadingFailed()
            : SingleChildScrollView(
                padding: EdgeInsets.zero,
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color.fromARGB(255, 38, 118, 200),
                        Colors.white,
                      ],
                    ),
                  ),
                  child: Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.only(top: 60, left: 20),
                          alignment: Alignment.topLeft,
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/ic_location.png",
                                width: 18,
                                height: 20,
                              ),
                              const Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text("长沙",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18)),
                              )
                            ],
                          )),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 50, top: 20),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 5),
                              child: Text(dayWeatherInfo?.type ?? "未知",
                                  style: const TextStyle(
                                      color: Colors.white, fontSize: 18)),
                            ),
                            Text(
                                "最高${dayWeatherInfo?.tempMax} 最低${dayWeatherInfo?.tempMin}",
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 14)),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 10, bottom: 10),
                              child: Text("空气${dayWeatherInfo?.aqiDesc}",
                                  style: const TextStyle(
                                      color: Colors.white, fontSize: 14)),
                            ),
                            Text(nowWeatherInfo?.temp ?? "",
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 48)),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: Card(
                          color: const Color(0x66000000),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.0), // 设置圆角半径
                          ),
                          child: const Weather24HoursTemperatureTab(),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          color: const Color(0x66000000),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.0), // 设置圆角半径
                          ),
                          child: const Weather7DaysTab(),
                        ),
                      ),
                    ],
                  ),
                ),
              );
  }
}
