import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/components/custom_bottom_navigation_bar.dart';
import 'package:openvalley_smart_agriculture/pages/base_page.dart';
import 'package:openvalley_smart_agriculture/pages/news_page.dart';
import 'package:openvalley_smart_agriculture/pages/weather_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PageController controller = PageController(); // 初始化控制器

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  void _onTabTapped(int index) {
    controller.animateToPage(index,
        duration: const Duration(milliseconds: 500),
        curve: Curves.easeInOutSine);
  }

  GlobalKey<CustomBottomNavigationBarState> childWidgetKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: [
          Flexible(
            child: PageView(
              physics: const NeverScrollableScrollPhysics(),
              onPageChanged: (i) {
                // 在跨页点击的时候，有显示的小bug
                // setState(() {
                //   childWidgetKey.currentState?.setIndex(i);
                // });
              },
              controller: controller,
              children: const <Widget>[
                Center(
                  child: BasePage(),
                ),
                Center(
                  child: WeatherPage(),
                ),
                Center(
                  child: NewsPage(),
                ),
                // Container(color: Colors.green),
                // Container(color: Colors.green),
              ],
            ),
          ),
          CustomBottomNavigationBar(key: childWidgetKey, onTap: _onTabTapped),
        ],
      ),
    );
  }
}
