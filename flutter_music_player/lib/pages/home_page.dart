import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import '../utils/LogUtil.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class MusicInfo {
  int index = 0;
  String name = "";
  String path = "";

  MusicInfo(this.index, this.name, this.path);
}

List<MusicInfo> musicList = [
  MusicInfo(0, "Dream It Possible", "Dream_It_Possible.mp3"),
  MusicInfo(1, "Piano Man - Billy Joel", "english.mp3"),
  MusicInfo(2, "消化不良 - 周国贤", "yueyu.mp3"),
  MusicInfo(3, "听妈妈的话 - 周杰伦", "mama.mp3"),
  MusicInfo(4, "有何不可 - 许嵩", "youhebuke.mp3"),
  MusicInfo(5, "父亲写的散文诗 - 许飞", "fqdsws.mp3"),
];

enum ActionType { none, play, pause, next, previous, dragging, seek, completed }

class _HomePageState extends State<HomePage> {
  //当前播放的歌曲信息
  MusicInfo? _currentMusic;

  //在资源准备好后，是否自动播放
  bool _autoPlay = false;

  //当前是否正在播放
  bool _playing = false;

  //当前播放时间，单位s
  int _timeI = 0;

  //当前播放总时长，单位s
  int _durationI = 0;

  //当前播放时间格式化
  String _curTime = "00:00";

  //当前播放总时长格式化
  String _duration = "00:00";

  //当前进度百分比
  double _progress = 0.0;

  //新增这个变量，在seek期间，防止进度条抖动
  double _nextSeekValue = 0.0;

  //在一系列操作完成之前，锁定其他操作，涉及锁定检查的操作主要是用户操作： 播放、下一首、上一首、拖动进度条
  ActionType _actionType = ActionType.none;

  bool isLock() {
    return _actionType != ActionType.none;
  }

  void lock(ActionType actionType) {
    _actionType = actionType;
  }

  void unlock() {
    _actionType = ActionType.none;
  }

  //播放完成后，是否自动播放下一首
  final bool _autoNextOne = true;

  String paddingZero(int num) {
    if (num < 10) {
      return "0$num";
    } else {
      return num.toString();
    }
  }

  //plugin接口
  static const MethodChannel _channel =
      MethodChannel('AVPlayer', JSONMethodCodec());

  @override
  void initState() {
    super.initState();
    // 设置回调函数
    _channel.setMethodCallHandler((call) async {
      String method = call.method; // 获取方法名
      Map<String, dynamic> mapValue = call.arguments; // 获取参数

      switch (method) {
        //更新进度
        case 'updateTime':
          _timeI = (mapValue['time'] / 1000).toInt();
          _durationI = (mapValue['duration'] / 1000).toInt();
          Log.info("time=$_timeI");
          Log.info("duration=$_durationI");
          int min = _timeI ~/ 60;
          int sec = _timeI % 60;
          int min1 = _durationI ~/ 60;
          int sec1 = _durationI % 60;
          setState(() {
            _curTime = "${paddingZero(min)}:${paddingZero(sec)}";
            _duration = "${paddingZero(min1)}:${paddingZero(sec1)}";
            //拖拽和跳转过程中不更新progress
            if (_actionType != ActionType.dragging &&
                _actionType != ActionType.seek) {
              bool progressValid = false;
              double newProgress = _timeI / _durationI;
              if (_durationI != 0 && _timeI < _durationI) {
                //如果_nextSeekValue不为0，只在0.2范围内生效
                if (_nextSeekValue != 0.0) {
                  if ((_nextSeekValue - newProgress).abs() < 0.2) {
                    _nextSeekValue = 0.0;
                    progressValid = true;
                  } else {
                    Log.info("当前progress不生效：$newProgress");
                  }
                } else {
                  progressValid = true;
                }
              }
              if (progressValid) {
                _progress = newProgress;
              }
            }
          });
          break;
        //状态变更
        case "notifyStateChange":
          String state = mapValue['state'];
          onStateChange(state);
          break;
      }
    });

    //准备第一首歌，不自动播放
    prepareMusicInfo(musicList.first, false);
  }

  @override
  void dispose() {
    super.dispose();
    release();
  }

  /// 状态变更监听
  void onStateChange(String state) {
    Log.info("收到状态通知,$state");
    switch (state) {
      case 'idle': // 成功调用reset接口后触发该状态机上报
        break;
      case 'initialized': // avplayer 设置播放源后触发该状态上报
        //初始化好了，开始准备
        prepare();
        break;
      case 'prepared': // prepare调用成功后上报该状态机
        //准备好了，开始播放
        if (_autoPlay) {
          playOrPause();
        } else {
          //否则获取播放信息
          getMusicDuration().then((duration) => {
                Future.microtask(() {
                  _durationI = duration ~/ 1000;
                  int min1 = _durationI ~/ 60;
                  int sec1 = _durationI % 60;
                  setState(() {
                    // 更新状态
                    _duration = "${paddingZero(min1)}:${paddingZero(sec1)}";
                    _progress = 0.0;
                  });
                })
              });
        }
        break;
      case 'playing': // play成功调用后触发该状态机上报
        setState(() {
          _playing = true;
        });
        if ([
          ActionType.play,
          ActionType.next,
          ActionType.previous,
          ActionType.completed
        ].contains(_actionType)) {
          unlock();
        }
        break;
      case 'paused': // pause成功调用后触发该状态机上报
        setState(() {
          _playing = false;
        });
        if (ActionType.pause == _actionType) {
          unlock();
        }
        break;
      case 'completed': // 播放结束后触发该状态机上报
        if (_autoNextOne) {
          //自动播放下一首
          lock(ActionType.completed);
          prepareMusicInfo(getNextMusic(), true);
        } else {
          setState(() {
            _playing = false;
          });
        }
        break;
      case 'stopped': // stop接口成功调用后触发该状态机上报
        switch (_actionType) {
          case ActionType.next:
            //自动播放下一首
            prepareMusicInfo(getNextMusic(), true);
            break;
          case ActionType.previous:
            //自动播放上一首
            prepareMusicInfo(getPreviousMusic(), true);
            break;
          default:
            //只在默认情况，暂停状态栏，切换歌曲，不停止playing
            setState(() {
              _playing = false;
            });
            break;
        }
        break;
      case 'released':
        setState(() {
          _playing = false;
        });
        break;
      default:
        break;
    }
  }

  /// 获取上一首歌曲信息
  MusicInfo getPreviousMusic() {
    if (_currentMusic == null) {
      return musicList.first;
    }
    int current = _currentMusic!.index;
    int next = (current - 1) % musicList.length;
    return getMusicFromIndex(next);
  }

  MusicInfo getNextMusic() {
    if (_currentMusic == null) {
      return musicList.first;
    }
    int current = _currentMusic!.index;
    int next = (current + 1) % musicList.length;
    return getMusicFromIndex(next);
  }

  MusicInfo getMusicFromIndex(int index) {
    for (var value in musicList) {
      if (value.index == index) {
        return value;
      }
    }
    return musicList.first;
  }

  ///用户操作行为----
  void playOrPauseAction() {
    if (isLock()) {
      return;
    }
    if (_playing) {
      lock(ActionType.pause);
    } else {
      lock(ActionType.play);
    }
    playOrPause();
  }

  void nextAction() {
    if (isLock()) {
      return;
    }
    lock(ActionType.next);
    //先停止这一首，再开始下一首
    stop();
  }

  void previousAction() {
    if (isLock()) {
      return;
    }
    lock(ActionType.previous);
    //先停止这一首，再开始下一首
    stop();
  }

  void seekAction(double value) {
    if (isLock()) {
      return;
    }
    if (_durationI <= 0) {
      Log.severe("跳转进度错误，_durationI<=0");
      return;
    }
    lock(ActionType.seek);
    _nextSeekValue = value;
    var seek = (value * _durationI * 1000).toInt();
    //seekValue传ms值
    _channel.invokeMethod("seek", {"seekValue": seek});
    unlock();
  }

  ///用户操作行为----end

  void playOrPause() {
    Log.info("playOrPause()");
    switch (_actionType) {
      case ActionType.pause:
        _channel.invokeMethod("pause");
        break;
      case ActionType.play:
        _channel.invokeMethod("play");
        break;
      case ActionType.next:
        _channel.invokeMethod("play");
        break;
      case ActionType.previous:
        _channel.invokeMethod("play");
        break;
      case ActionType.completed:
        _channel.invokeMethod("play");
        break;
    }
  }

  void start(String path) {
    Log.info("start()");
    _channel.invokeMethod('start', {"path": path});
  }

  void pause() {
    Log.info("pause()");
    _channel.invokeMethod("pause");
  }

  void previous() {
    Log.info("previous()");
    stop();
    //自动播放下一首
    prepareMusicInfo(getPreviousMusic(), true);
  }

  void next() {
    //先停止当前，再播放下一首
    Log.info("next()");
    stop();
    //自动播放下一首
    prepareMusicInfo(getNextMusic(), true);
  }

  void stop() {
    Log.info("stop()");
    _channel.invokeMethod("stop");
  }

  void prepare() {
    Log.info("开始准备");
    _channel.invokeMethod("prepare");
  }

  void release() {
    Log.info("释放资源");
    _channel.invokeMethod("release");
  }

  /// 准备某首歌的播放
  void prepareMusicInfo(MusicInfo musicInfo, bool autoPlay) {
    setState(() {
      _currentMusic = musicInfo;
    });
    _autoPlay = autoPlay;
    start(musicInfo.path);
  }

  ///获取音乐进度
  Future<int> getMusicDuration() async {
    int result = await _channel.invokeMethod("getDuration");
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bg_blurry.png"),
                fit: BoxFit.fill)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 66, left: 20, right: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:  [
                  const Text(
                    '音乐',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                  Image.asset('assets/images/ov_logo.png',
                  height: 30,
                  width: 70,),
                ],
              ),
            ),
            Expanded(child: Container()),
            Padding(
              padding: const EdgeInsets.only(bottom: 50),
              child: Text(
                _currentMusic?.name ?? "歌曲名",
                style: const TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 45, right: 45, bottom: 78),
              child: Image.asset("assets/images/album.png"),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25, right: 25, bottom: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    _curTime,
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                  Text(_duration,
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w500))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, bottom: 58),
              child: Slider(
                value: _progress,
                onChanged: (newValue) {
                  setState(() {
                    _progress = newValue;
                  });
                  Log.info("onChanged()$newValue");
                },
                onChangeStart: (newValue) {
                  Log.info("onChangeStart()$newValue");
                  //开始拖拽
                  lock(ActionType.dragging);
                },
                onChangeEnd: (newValue) {
                  Log.info("onChangeEnd()$newValue");
                  //先解开dragging的锁
                  unlock();
                  seekAction(newValue);
                },
                inactiveColor: const Color(0xffb3d4dc),
                activeColor: const Color(0xff067af4),
                thumbColor: Colors.white,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 76, left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    child: SvgPicture.asset(
                      "assets/images/ic_play_previous.svg",
                      width: 68,
                      height: 68,
                    ),
                    onTap: () => previousAction(),
                  ),
                  GestureDetector(
                    child: SvgPicture.asset(
                      _playing
                          ? "assets/images/ic_pause.svg"
                          : "assets/images/ic_play.svg",
                      width: 68,
                      height: 68,
                    ),
                    onTap: () => playOrPauseAction(),
                  ),
                  GestureDetector(
                      child: SvgPicture.asset(
                        "assets/images/ic_play_next.svg",
                        width: 68,
                        height: 68,
                      ),
                      onTap: () => nextAction()),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
